from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth import get_user_model
from django.contrib.sites.models import Site
from allauth.socialaccount.models import (
    SocialApp
)
import os

# dev-instance
# TWITCH_APP_NAME = os.environ.get('TWITCH_APP_NAME', 'TestApp1')
# TWITCH_CLIENT_ID = os.environ.get('TWITCH_CLIENT_ID','re0casy4z9x5igjnwctoy9zmxkh85x')
# TWITCH_CLIENT_SECRET = os.environ.get('TWITCH_CLIENT_SECRET', 'g8cmgnnaw48q2lbjxb26enad05dz43')
TWITCH_APP_NAME = os.environ.get('TWITCH_APP_NAME', 'TestApp1')
TWITCH_CLIENT_ID = os.environ.get('TWITCH_CLIENT_ID', 'cladybhwc62druvmpl87ciuehg63h4')
TWITCH_CLIENT_SECRET = os.environ.get('TWITCH_CLIENT_SECRET', 'e34ldlehnbnfljnihtc2f0dvt1v9m7')

print(TWITCH_APP_NAME)
print(TWITCH_CLIENT_ID)
print(TWITCH_CLIENT_SECRET)


class Command(BaseCommand):
    help = 'Creating initial social accounts'

    def handle(self, *args, **options):
        if SocialApp.objects.filter(provider='twitch').count() == 0:
            social_app = SocialApp.objects.create(provider="twitch", name=TWITCH_APP_NAME,
                                                  client_id=TWITCH_CLIENT_ID,
                                                  secret=TWITCH_CLIENT_SECRET, key="")
            # social_app.save()
            social_app.sites.add(Site.objects.get(id=1))
            self.stdout.write(self.style.SUCCESS('Successfully created Twitch Account'))
        else:
            self.stdout.write(self.style.SUCCESS('Twitch App already created.'))
