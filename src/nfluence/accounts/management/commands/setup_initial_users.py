from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth import get_user_model
from accounts.models import (
    UserProfile, CompanyUserProfile
)
from company.models import (
    Company
)
from django.contrib.auth.models import Group, Permission

User = get_user_model()
GROUPS = ['admin', 'viewer', 'influencer']


class Command(BaseCommand):
    help = 'Creating initial users and profiles'

    def handle(self, *args, **options):
        if Group.objects.count() == 0:
            for group in GROUPS:
                new_group, created = Group.objects.get_or_create(name=group)

            self.stdout.write(self.style.SUCCESS('Successfully created groups'))
        else:
            self.stdout.write(self.style.SUCCESS('New groups creation not needed.'))

        if User.objects.count() == 0:

            # To create a company User
            company = Company.objects.create(name="Company 1", address_line_1="Company address line 1", city="Lahore",
                                             zip_code="54000")
            self.stdout.write(self.style.SUCCESS('Successfully created company 1'))

            # Create first company user
            user_profile = UserProfile.objects.create()
            CompanyUserProfile.objects.create(company=company, company_email="user1@company1.com",
                                              user_profile=user_profile)

            user = User.objects.create(email="user1@company1.com", username="company1_user_1", first_name="company1",
                                       last_name="user1", profile=user_profile)
            user.set_password("user1234")
            user.save()
            group = Group.objects.get(name='admin')
            user.groups.add(group)
            self.stdout.write(self.style.SUCCESS('Successfully created company user 1'))

            # Create second company user
            user_profile = UserProfile.objects.create()
            CompanyUserProfile.objects.create(company=company, company_email="user2@company1.com",
                                              user_profile=user_profile)

            user = User.objects.create(email="user2@company1.com", username="company1_user_2", first_name="company1",
                                       last_name="user2", profile=user_profile)
            user.set_password("user1234")
            user.save()
            group = Group.objects.get(name='viewer')
            user.groups.add(group)
            self.stdout.write(self.style.SUCCESS('Successfully created company user 2'))

            # Create a super User
            user_profile = UserProfile.objects.create()
            user = User.objects.create_user(email="admin1@admin.com", username="admin1", first_name="admin1",
                                            last_name="admin1", profile=user_profile, is_staff=True, is_superuser=True)
            user.set_password("admin1234")
            user.save()
            self.stdout.write(self.style.SUCCESS('Successfully created admin 1'))
        else:
            self.stdout.write(self.style.SUCCESS('New users creation not needed.'))
