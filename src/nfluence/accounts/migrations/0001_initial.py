# Generated by Django 2.2.5 on 2020-01-28 10:25

from django.conf import settings
import django.contrib.auth.models
import django.contrib.postgres.fields.citext
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('first_name', models.CharField(blank=True, max_length=30, verbose_name='first name')),
                ('last_name', models.CharField(blank=True, max_length=150, verbose_name='last name')),
                ('is_staff', models.BooleanField(default=False, help_text='Designates whether the user can log into this admin site.', verbose_name='staff status')),
                ('is_active', models.BooleanField(default=True, help_text='Designates whether this user should be treated as active. Unselect this instead of deleting accounts.', verbose_name='active')),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date joined')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_on', models.DateTimeField(auto_now=True, null=True)),
                ('username', django.contrib.postgres.fields.citext.CICharField(max_length=50, unique=True)),
                ('email', django.contrib.postgres.fields.citext.CIEmailField(blank=True, max_length=254, null=True, verbose_name='email address')),
            ],
            options={
                'verbose_name': 'user',
                'verbose_name_plural': 'users',
                'abstract': False,
            },
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.CreateModel(
            name='CompanyUserProfile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_on', models.DateTimeField(auto_now=True, null=True)),
                ('company_email', django.contrib.postgres.fields.citext.CIEmailField(blank=True, max_length=254, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_on', models.DateTimeField(auto_now=True, null=True)),
                ('profile_avatar', models.ImageField(blank=True, null=True, upload_to='profile_avatars/')),
                ('display_name', models.CharField(blank=True, max_length=150, verbose_name='full name')),
                ('full_name', models.CharField(blank=True, max_length=70, null=True)),
                ('state', django.contrib.postgres.fields.citext.CICharField(blank=True, max_length=255, null=True)),
                ('country', django.contrib.postgres.fields.citext.CICharField(blank=True, max_length=255, null=True)),
                ('zip_code', django.contrib.postgres.fields.citext.CICharField(blank=True, max_length=255, null=True)),
                ('email', django.contrib.postgres.fields.citext.CIEmailField(blank=True, max_length=70, null=True)),
                ('phone_number', django.contrib.postgres.fields.citext.CICharField(blank=True, max_length=255, null=True)),
                ('gaming_genre', django.contrib.postgres.fields.citext.CICharField(blank=True, max_length=255, null=True)),
                ('social_media_pages', django.contrib.postgres.fields.citext.CICharField(blank=True, max_length=255, null=True)),
                ('twitch_api_key', django.contrib.postgres.fields.citext.CICharField(blank=True, max_length=255, null=True)),
                ('created_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='userprofile_createdby', to=settings.AUTH_USER_MODEL)),
                ('updated_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='userprofile_modifiedby', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='InfluencerUserProfile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_on', models.DateTimeField(auto_now=True, null=True)),
                ('influencer_rate', models.IntegerField(default=0)),
                ('average_viewer', models.IntegerField(default=0)),
                ('influencer_email', django.contrib.postgres.fields.citext.CIEmailField(blank=True, max_length=254, null=True)),
                ('paypal_account', django.contrib.postgres.fields.citext.CICharField(blank=True, max_length=255, null=True)),
                ('created_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='influenceruserprofile_createdby', to=settings.AUTH_USER_MODEL)),
                ('updated_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='influenceruserprofile_modifiedby', to=settings.AUTH_USER_MODEL)),
                ('user_profile', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='accounts.UserProfile')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
