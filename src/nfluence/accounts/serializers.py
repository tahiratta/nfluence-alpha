from rest_framework import serializers
from rest_framework.fields import empty
from django.contrib.auth import get_user_model
from accounts.models import (
    UserProfile,
    CompanyUserProfile, InfluencerUserProfile
)
from influencers.models import (
    InfluencerCampaign, InfluencerCampaignSubmission
)
from payments.models import (
    Escrow
)
from campaigns.serializers import (
    CampaignSerializer
)
from django.contrib.auth.models import Group

User = get_user_model()


class UserProfileSerializer(serializers.ModelSerializer):
    # company_user_profile = CompanyUserProfileSerializer(required=False)
    username = serializers.SerializerMethodField(read_only=True)
    full_name = serializers.CharField(required=False)
    # full_name = serializers.SerializerMethodField(read_only=True)
    user_id = serializers.SerializerMethodField(read_only=True)

    @staticmethod
    def get_username(obj):
        return obj.user.username

    # @staticmethod
    # def get_full_name(obj):
    #     return obj.user.get_full_name()

    @staticmethod
    def get_user_id(obj):
        return obj.user.id

    class Meta:
        model = UserProfile
        fields = (
            'id', 'profile_avatar', 'display_name', 'state', 'country', 'zip_code', 'email', 'phone_number', 'username',
            'full_name', 'user_id', 'gaming_genre', 'social_media_pages', 'twitch_api_key')


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ('name',)


class CompanyUserProfileSerializer(serializers.ModelSerializer):
    user_profile = UserProfileSerializer(required=True)

    class Meta:
        model = CompanyUserProfile
        fields = ('id', 'company_email', 'company', 'user_profile')


class InfluencerUserProfileSerializer(serializers.ModelSerializer):
    user_profile = UserProfileSerializer(required=True)
    campaigns = serializers.SerializerMethodField()
    user_status = serializers.SerializerMethodField()

    @staticmethod
    def get_campaigns(obj):
        campaigns = list()

        passed_campaigns = list()

        influencer_campaigns = InfluencerCampaign.objects.filter(influencer_user_profile=obj).order_by('-id')

        for ic in influencer_campaigns:
            if ic.campaign.id not in passed_campaigns:
                user = User.objects.get(profile=ic.influencer_user_profile.user_profile)
                escrow = Escrow.objects.filter(influencer_user_profile=obj,
                                               campaign=ic.campaign).order_by('-id')

                passed_campaigns.append(ic.campaign.id)

                ic.campaign.influencer_status = ic.status

                submission = InfluencerCampaignSubmission.objects.filter(influencer_user_profile=obj, campaign=ic.campaign)
                if submission.count() > 0:
                    submission = submission[0]
                    ic.campaign.submitted_media = submission.submitted_media()
                else:
                    ic.campaign.submitted_media = None

                campaign = CampaignSerializer(ic.campaign).data
                campaigns.append(campaign)

        return campaigns

    @staticmethod
    def get_user_status(obj):
        return obj.user_profile.user.get_user_status()

    class Meta:
        model = InfluencerUserProfile
        fields = (
            'id', 'influencer_rate', 'average_viewer', 'user_profile', 'influencer_email', 'w9_form_submitted', 'paypal_account',
            'campaigns', 'user_status')
        read_only_fields = ('campaigns', 'user_status')

    def update(self, instance, validated_data):
        w9_form_submitted = validated_data.get('w9_form_submitted')
        instance.w9_form_submitted = w9_form_submitted
        instance.save()
        return instance



class UserSerializer(serializers.ModelSerializer):
    def __init__(self, instance=None, data=empty, **kwargs):
        if instance:
            setattr(self.Meta, 'depth', 1)
        else:
            setattr(self.Meta, 'depth', 0)
        super(UserSerializer, self).__init__(instance, data, **kwargs)

    profile = UserProfileSerializer(required=False)
    user_type = serializers.SerializerMethodField(read_only=True)
    # groups = GroupSerializer(many=True, required=False)
    company_user_profile = serializers.DictField(required=False)

    class Meta:
        depth = 0
        model = User
        fields = (
            'id', 'url', 'first_name', 'last_name', 'password', 'username', 'email', 'profile', 'user_type',
            'company_user_profile', 'groups', 'is_active'
        )
        # read_only_fields = ('groups', )
        extra_kwargs = {
            'password': {'write_only': True},
            'company_user_profile': {'write_only': True}
        }

    @staticmethod
    def get_user_type(obj):
        return obj.get_user_type()

    def create(self, validated_data):
        groups_data = validated_data.pop('groups')

        user = None
        request = self.context.get("request")
        current_user_company = None
        if request and hasattr(request, "user"):
            current_user_company = request.user.profile.companyuserprofile.company

        company_profile_data = validated_data.pop('company_user_profile')
        profile_data = company_profile_data.pop('profile')

        profile = UserProfile.objects.create(**profile_data)

        company_profile = None
        company_profile = CompanyUserProfile.objects.create(**company_profile_data, user_profile=profile,
                                                            company=current_user_company)
        password = validated_data.pop('password')
        user = User(**validated_data, profile=profile)
        user.set_password(password)
        user.save()
        # profile.display_name = user.get_full_name()
        profile.save()

        for group_data in groups_data:
            # Group.objects.create(user=user, **group_data)
            user.groups.add(group_data)

        return user

    def update(self, instance, validated_data):
        # groups_data = validated_data.pop('groups')
        # profile_data = validated_data.pop('profile')


        profile = instance.profile

        password = validated_data.get('password', None)
        if password:
            instance.set_password(password)

        instance.email = validated_data.get('email', instance.email)
        instance.first_name = validated_data.get('first_name', instance.first_name)
        instance.last_name = validated_data.get('last_name', instance.last_name)
        instance.is_active = validated_data.get('is_active', instance.is_active)
        instance.save()

        # instance.groups.clear()
        # for group_data in groups_data:
        #     # Group.objects.create(user=user, **group_data)
        #     instance.groups.add(group_data)

        # profile.title = profile_data.get('title', profile.title)
        profile.save()

        return instance
