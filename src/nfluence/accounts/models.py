from django.utils.translation import ugettext_lazy as _

from django.db import models, transaction
from django.contrib.postgres.fields import citext
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import Group

from allauth.account.signals import user_signed_up
from django.dispatch import receiver

from company.models import Company
from nfluence.models import NfluenceBaseModel
from django.db.models.signals import pre_save
from django.core.files.storage import FileSystemStorage
from django.conf import settings
UPLOAD_ROOT = settings.BASE_DIR + "/media_1/web_media/"
upload_storage = FileSystemStorage(location=UPLOAD_ROOT, base_url='/web_media_1')

class UserProfile(NfluenceBaseModel):
    #profile_avatar = models.ImageField(upload_to='profile_avatars/', blank=True, null=True)
    profile_avatar = models.ImageField(upload_to='profile_avatars/', storage=upload_storage, blank=True, null=True)

    display_name = models.CharField(_('full name'), max_length=150, blank=True)
    full_name = models.CharField(max_length=70, blank=True, null=True)

    state = citext.CICharField(max_length=255, blank=True, null=True)
    country = citext.CICharField(max_length=255, blank=True, null=True)
    zip_code = citext.CICharField(max_length=255, blank=True, null=True)

    email = citext.CIEmailField(max_length=70, blank=True, null=True)
    phone_number = citext.CICharField(null=True, blank=True, max_length=255)
    gaming_genre = citext.CICharField(null=True, blank=True, max_length=255)
    social_media_pages = citext.CICharField(null=True, blank=True, max_length=255)
    twitch_api_key = citext.CICharField(null=True, blank=True, max_length=255)

    def __str__(self):
        return str(self.user.email)


@receiver(pre_save, sender=UserProfile)
def delete_old_image(sender, instance, *args, **kwargs):
    if instance.pk:
        existing_image = UserProfile.objects.get(pk=instance.pk)
        if instance.profile_avatar and existing_image.profile_avatar != instance.profile_avatar:
            existing_image.profile_avatar.delete(False)
    else:
        pass


class CompanyUserProfile(NfluenceBaseModel):
    company_email = citext.CIEmailField(null=True, blank=True)
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    user_profile = models.OneToOneField(UserProfile, on_delete=models.CASCADE)

    def __str__(self):
        return self.company_email if self.company_email else self.user_profile.user.username

    def __repr__(self):
        return self.company_email if self.company_email else self.user_profile.user.username


class InfluencerUserProfile(NfluenceBaseModel):
    influencer_rate = models.IntegerField(default=0)
    average_viewer = models.IntegerField(default=0)
    influencer_email = citext.CIEmailField(null=True, blank=True)
    w9_form_submitted = models.BooleanField(default=False)
    paypal_account = citext.CICharField(max_length=255, blank=True, null=True)
    user_profile = models.OneToOneField(UserProfile, on_delete=models.CASCADE)

    def __str__(self):
        return self.influencer_email if self.influencer_email else self.user_profile.user.username

    def __repr__(self):
        return self.influencer_email if self.influencer_email else self.user_profile.user.username

    def get_twitch_username(self):
        return self.user_profile.user.username


class User(AbstractUser, NfluenceBaseModel):
    username = citext.CICharField(max_length=50, unique=True)
    email = citext.CIEmailField(_('email address'), null=True, blank=True)

    profile = models.OneToOneField(UserProfile, on_delete=models.CASCADE)

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email', 'first_name', 'last_name', 'profile']

    def __str__(self):
        return "{}".format(self.username)

    def get_user_type(self):
        if CompanyUserProfile.objects.filter(user_profile=self.profile):
            return 'company'
        elif InfluencerUserProfile.objects.filter(user_profile=self.profile):
            return 'influencer'
        else:
            return None

    def get_user_status(self):
        return 'Active' if self.is_active else 'Suspended'

    @transaction.atomic
    def save(self, *args, **kwargs):
        if not self.profile_id:
            self.profile = UserProfile.objects.create()

        super(User, self).save(*args, **kwargs)


@receiver(user_signed_up)
def setup_influencer_profile(sender, **kwargs):
    # username = kwargs['request'].user.username
    social_login = kwargs.get('sociallogin')
    user = social_login.user
    if user:
        try:
            user_profile = user.profile
        except Exception:
            user_profile = None
        if not user_profile:
            # Create UserProfile
            user.profile = UserProfile.objects.create()
            user.save()
        user_profile = user.profile
        # Check if Influencer profile exists
        if InfluencerUserProfile.objects.filter(user_profile=user_profile).count() == 0:
            # Create InfluencerUser Profile
            iup = InfluencerUserProfile.objects.create(user_profile=user_profile)
            group = Group.objects.get(name='influencer')
            user.groups.add(group)
