from rest_framework import viewsets
from rest_framework import views
from rest_framework.response import Response

#from allauth.socialaccount.providers.twitch.views import TwitchOAuth2Adapter
from .adapters import TwitchOAuth2Adapter
from rest_auth.registration.views import (
    SocialLoginView,
    SocialConnectView
)

from django.contrib.auth.models import Group

from accounts.models import User, InfluencerUserProfile, UserProfile
from accounts.serializers import UserSerializer, InfluencerUserProfileSerializer, UserProfileSerializer

from accounts.permissions import IsLoggedInUserOrAdmin, IsAdminUser


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def get_permissions(self):
        permission_classes = []
        if self.action == 'create':
            permission_classes = [IsAdminUser]
        elif self.action == 'retrieve' or self.action == 'update' or self.action == 'partial_update':
            permission_classes = [IsLoggedInUserOrAdmin]
        elif self.action == 'list' or self.action == 'destroy':
            permission_classes = [IsAdminUser]
        return [permission() for permission in permission_classes]


class UserProfileViewSet(viewsets.ModelViewSet):
    queryset = UserProfile.objects.all()
    serializer_class = UserProfileSerializer

    def get_permissions(self):
        permission_classes = []
        if self.action == 'create':
            permission_classes = [IsAdminUser]
        elif self.action == 'retrieve' or self.action == 'update' or self.action == 'partial_update':
            permission_classes = [IsLoggedInUserOrAdmin]
        elif self.action == 'list' or self.action == 'destroy':
            permission_classes = [IsAdminUser]
        return [permission() for permission in permission_classes]


class InfluencerViewSet(viewsets.ModelViewSet):
    queryset = InfluencerUserProfile.objects.all()
    serializer_class = InfluencerUserProfileSerializer

    # def get_permissions(self):
    #     permission_classes = []
    #     if self.action == 'create':
    #         permission_classes = [IsAdminUser]
    #     elif self.action == 'retrieve' or self.action == 'update' or self.action == 'partial_update':
    #         permission_classes = [IsLoggedInUserOrAdmin]
    #     elif self.action == 'list' or self.action == 'destroy':
    #         permission_classes = [IsAdminUser]
    #     return [permission() for permission in permission_classes]


class TwitchLogin(SocialLoginView):
    adapter_class = TwitchOAuth2Adapter


class TwitchConnect(SocialConnectView):
    adapter_class = TwitchOAuth2Adapter


class AssignUserGroupView(views.APIView):

    def post(self, request, *args, **kwargs):
        status = None
        response = {}

        user_id = None
        try:
            user_id = int(request.POST.get('user_id'))
        except ValueError as ve:
            status = False
            response['message'] = 'user_id must be a valid integer'

        group_name = request.POST.get('group_name', None)
        if group_name == '' or not group_name:
            status = False
            response['message'] = 'group name is required.'
        else:
            user = None
            try:
                user = User.objects.get(id=user_id)
            except User.DoesNotExist:
                status = False
                response['message'] = 'Invalid user ID'

            group = None
            try:
                group = Group.objects.get(name=group_name)
            except Group.DoesNotExist:
                status = False
                response['message'] = 'Invalid group name'

            if user_id and user and group:
                user.groups.clear()

                user.groups.add(group)

                status = True
                response['message'] = 'Group Updated'

        return Response({'status': status, 'response': response})

