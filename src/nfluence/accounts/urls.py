from django.urls import path, include

from rest_framework import routers

from rest_auth.registration.views import (
    SocialAccountListView, SocialAccountDisconnectView
)
from accounts.views import (
    UserViewSet, InfluencerViewSet, UserProfileViewSet,
    TwitchLogin, TwitchConnect,
    AssignUserGroupView
)

router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'influencers', InfluencerViewSet)
router.register(r'user_profiles', UserProfileViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('auth/', include('rest_auth.urls')),
    path('accounts/', include('allauth.urls')),

    path('auth/twitch/', TwitchLogin.as_view(), name='twitch_login'),
    path('auth/twitch/connect/', TwitchConnect.as_view(), name='twitch_connect'),

    path('socialaccounts/', SocialAccountListView.as_view(), name='social_account_list'),
    path('socialaccounts/<int:pk>/disconnect/', SocialAccountDisconnectView.as_view(),
         name='social_account_disconnect'),

    path('assign-user-group/', AssignUserGroupView.as_view(), name='assign_user_group')
]
