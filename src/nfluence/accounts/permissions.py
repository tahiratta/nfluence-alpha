from rest_framework import permissions


ADMIN_ROLE_NAME = 'admin'
INFLUENCER_ROLE_NAME = 'influencer'


class IsLoggedInUserOrAdmin(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        return obj == request.user or request.user.is_superuser or request.user.groups.filter(name=ADMIN_ROLE_NAME).exists() or request.user.groups.filter(name=INFLUENCER_ROLE_NAME).exists()


class IsAdminUser(permissions.BasePermission):

    def has_permission(self, request, view):
        return request.user and (request.user.is_superuser or request.user.groups.filter(name=ADMIN_ROLE_NAME).exists())

    def has_object_permission(self, request, view, obj):
        return request.user and (request.user.is_superuser or request.user.groups.filter(name=ADMIN_ROLE_NAME).exists())
