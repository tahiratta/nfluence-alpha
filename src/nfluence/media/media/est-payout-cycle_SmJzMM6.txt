Current twitch strategy:

1 - a person signs up as influencer, in sign up, person gives twitch user name2 - using twitch user name, nfluence does the following:

2.1  every 15 minutes - nfluence calls users API to get "total view count" as follows:  https://api.twitch.tv/helix/users?login=gabbieresults - where view_count is perpetual ( and view_count is used for overlay and underlay )NOTE:  API can be called with a list of users IDs, maximum 100 user IDs

{"data": [{"id": "77574036","login": "gabbie","display_name": "Gabbie","type": "","broadcaster_type": "partner","description": "Ba-dum-tssss","profile_image_url": "https://static-cdn.jtvnw.net/jtv_user_pictures/f236e1bb-edad-4c58-9fcb-87becf4d9a11-profile_image-300x300.jpg","offline_image_url": "https://static-cdn.jtvnw.net/jtv_user_pictures/27c232bf-d55c-412a-8279-95e126249ecb-channel_offline_image-1920x1080.png","view_count": 6686618}]} 

3. for overlay 3.1 - influencer submits clip URL3.2 - using clip url, parse to get clip ID

https://clips.twitch.tv/FrigidHonestTitanResidentSleeper

parse above to get clip ID:   FrigidHonestTitanResidentSleeper

3.3 - using clip ID, make API call to get video ID, example:

https://api.twitch.tv/helix/clips?id=FrigidHonestTitanResidentSleeper

results:

{"data": [{"id": "FrigidHonestTitanResidentSleeper","url": "https://clips.twitch.tv/FrigidHonestTitanResidentSleeper","embed_url": "https://clips.twitch.tv/embed?clip=FrigidHonestTitanResidentSleeper","broadcaster_id": "444358584","broadcaster_name": "kashif_cassandra","creator_id": "444358584","creator_name": "kashif_cassandra","video_id": "515920617","game_id": "","language": "en","title": "xbox1","view_count": 5,"created_at": "2019-12-01T23:46:06Z","thumbnail_url": "https://clips-media-assets2.twitch.tv/vod-515920617-offset-12-preview-480x272.jpg"}],"pagination": {}}

from results, get video_id == 515920617 and make api call 

https://api.twitch.tv/helix/videos?id=515920617

to get created time and duration as follows:

{"data": [{"id": "515920617","user_id": "444358584","user_name": "kashif_cassandra","title": "","description": "","created_at": "2019-12-01T23:43:07Z","published_at": "2019-12-01T23:43:07Z","url": "https://www.twitch.tv/videos/515920617","thumbnail_url": "https://static-cdn.jtvnw.net/s3_vods/c92ec634b2f2ab2af889_kashif_cassandra_36332849984_1339511279/thumb/thumb0-%{width}x%{height}.jpg","viewable": "public","view_count": 1,"language": "en","type": "archive","duration": "47s"}],"pagination": {}}

from created date time subtract duration to get start time of video, then using created_at as end time of video, lookup in nfluence db historythe stream count for the start time and end time of video.  subtract the end time view count from the start time view count to get the delta

then calculate payout as :  0.03 * delta

for underlay:  

start time is time influencers submits the campaign (change button lable for influencer, so that influencer clicks a "start" campaign button.  on server is same as submit)

and end time is campaign duration 

at end of campaign, lookup up view count for date of "start" or submit campaign as start view countand where end view count is view count for date of end of campaign duration

during underlay duration, nfluence checks twitch user page for underlay to exist (lookup underlay html table for logo/img, and url match)nfluence uses beautiful soap to go to url like:   https://www.twitch.tv/kashif_cassandra/   to look for underlay elements

then calculate payout as : 0.01 * delta

for voice/audiosame as overlay, lookup clip via API to get videothen lookup video to get created at (as end time) and duration from created at (to get start time)then lookup in DB "concurrent" view count for stream for start time and end time every 15 minutes...where NOTE: another background process is making API stream calls like below and saving "viewer_count" as "concurrent viewers" in history

https://api.twitch.tv/helix/streams?user_id=77574036

{"data": [{"id": "36339010672","user_id": "178046642","user_name": "D__Rich","game_id": "493057","type": "live","title": "Scrims scrims scrims scrims","viewer_count": 22,"started_at": "2019-12-03T00:10:28Z","language": "en","thumbnail_url": "https://static-cdn.jtvnw.net/previews-ttv/live_user_d__rich-{width}x{height}.jpg","tag_ids": ["6ea6bca4-4712-4ab9-a906-e3336a9d8039","f52ae90d-941c-447e-bef1-558aea0165b8"]}],"pagination": {"cursor": "eyJiIjpudWxsLCJhIjp7Ik9mZnNldCI6MX19"}}

to take average, drop lowest concurrent viewers value, and average the remaining countsthen calculate payout as:  0.10 * average of concurrent viewers

db table structure:1 - table/column to store streams API "concurrent viewers" == streams "viewer_count"2 - table/column to store twitch user "view_count"  == total views over time

0 - twitch API limits:

https://dev.twitch.tv/docs/api/guide/

Rate LimitsTo prevent our API from being overwhelmed by too many requests, Twitch rate-limits requests. A token bucketalgorithm is used, where a token (aka point) counts for a request. The refill rate is set on a steady-state rate and the burst is the maximum bucket size.

Each client ID has a point-refill rate and bucket size which depend on whether a bearer token is provided:

Authentication	Refill Rate	Bucket SizeBearer token is provided	800 points per minute, per user	800 pointsBearer token is not provided	30 points per minute	30 pointsThe limit is across all New Twitch API queries. If this limit is exceeded, an error is returned: HTTP 429 (Too Many Requests).

When you make an API request, you will receive a set of rate-limiting headers to help your application respond appropriately. The headers are:

Ratelimit-Limit — The rate at which points are added to your bucket. This is the average number of requests per minute you can make over an extended period of time.Ratelimit-Remaining — The number of points you have left to use.Ratelimit-Reset — A Unix epoch timestamp of when your bucket is reset to full.Individual endpoints may have additional rate limits, with additional headers to describe those limits. For details, see the documentation for each endpoint.

Individual endpoints are given a point value. When an endpoint is called, the point value of the endpoint is subtracted from the total points granted to your client ID. The default point value is 1; i.e., a point is equivalent to a request. (All New Twitch API endpoints have the default point value. In the future, if an endpoint has a higher point value, that will be covered in the reference documentation for the endpoint.)

If you need a higher rate limit, please fill out the form at https://dev.twitch.tv/limit-increase.

800 API calls per minute.   if nfluencer makes the following API calls:

user ( every 15 minutes )stream ( every 15 minutes )clip - one on demand/submissionvideo - one on demand/submission

then nfluence can make 200 calls each per minute for 800 API calls per minute.each API call can support multiple IDs

thus, for example if nfluence makes 200 user API calls, and each API call has 100 user IDs in the URL, then nfluence can lookup 20,000  users per minute.  if nfluence is looking up a user every 15 minutes, then nfluence can lookup 20,000 * 15 == 300,000 users every 15 minutes.
