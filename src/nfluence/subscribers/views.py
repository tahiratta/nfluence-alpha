from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import AllowAny

from subscribers.serializers import SubscriberSerializer


class SubscribersView(APIView):
    permission_classes = (AllowAny, )

    def post(self, request, *args, **kwargs):
        subscriber = request.data.get('subscriber')

        serializer = SubscriberSerializer(data=subscriber)
        if serializer.is_valid(raise_exception=True):
            subscriber_saved = serializer.save()
            return Response({"success": "Sign up Successful. Thank you"})
        else:
            return Response({"error": "Failed to save the email!"})
