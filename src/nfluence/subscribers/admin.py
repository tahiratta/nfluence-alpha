from django.contrib import admin
from subscribers.models import Subscriber


@admin.register(Subscriber)
class CampaignAdmin(admin.ModelAdmin):

    list_display = ('email',)
    # add filtering by campaign_budget
    list_filter = ('email',)
    # add search fields
    search_fields = ['email']
