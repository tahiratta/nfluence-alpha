from django.db import models
from django.contrib.postgres.fields import citext
from nfluence.models import NfluenceBaseModel


class Subscriber(NfluenceBaseModel):
    email = citext.CIEmailField(unique=True)
    twitch_id = models.IntegerField(unique=True, null=True, blank=True)

    def __str__(self):
        return str(self.email)

    def __repr__(self):
        return str(self.email)
