from django.urls import path
from subscribers.views import (
    SubscribersView
)


urlpatterns = [
    path('subscribe/', SubscribersView.as_view())
]
