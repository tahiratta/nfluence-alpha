#!/usr/bin/env bash

# for static files
python manage.py collectstatic --no-input

# for database migrations
python manage.py makemigrations
python manage.py migrate

# for initial setup
python manage.py setup_initial_users
python manage.py setup_social_accounts
python manage.py loaddata brands
python manage.py loaddata campaigns

# for running the server
#gunicorn nfluence.wsgi -b 0.0.0.0:8080 --timeout 600 
#gunicorn nfluence.wsgi -b 0.0.0.0:8080 --timeout 600 --workers 4 --log-level=debug  --log-file "/var/log/gunicorn.log" 
#gunicorn nfluence.wsgi -b 0.0.0.0:8080 --timeout 600 --workers 4 --threads 2 --log-level=debug  --log-file "/var/log/gunicorn.log" 
#gunicorn nfluence.wsgi -b 0.0.0.0:8080 --timeout 600 --workers 4
gunicorn nfluence.wsgi -b 0.0.0.0:8080 --worker-class eventlet --timeout 600 --workers 2
