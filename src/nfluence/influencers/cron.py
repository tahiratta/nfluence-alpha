from influencers.utils import (
    payment_validation,
    get_influencer_view_count,
    avg_estimated_payout_for_all_influencers,
    get_underlay_result,
    check_underlay_timeout,
    process_under_review_campaigns,
    close_trouble_ticket,
    mature_payment_after_net_days,
    paypal_batch_payout
)
import datetime
import time
from rest_framework_jwt.settings import api_settings
from accounts.models import (
    User
)


def scheduled_job():
    print("Payment Cron called!", str(datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')))
    # print(str(datetime.time))
    payment_validation()


def scheduled_twitch_job():
    print("Twitch View Count Cron called!",
          str(datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')))
    # print(str(datetime.time))
    get_influencer_view_count()
    avg_estimated_payout_for_all_influencers()

def scheduled_underlay_job():
    print("Underlay Timeout Cron called!", str(datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')))
    check_underlay_timeout()
    print("Underlay Cron called!", str(datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')))
    # print(str(datetime.time))
    get_underlay_result()


def scheduled_under_review_campaigns():
    print("Under Review Campaigns Cron called!", str(datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')))
    jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
    jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
    # print('Users detail', User.objects.filter(groups__name='influencer'))
    # print('First object of user is', User.objects.filter(groups__name='influencer')[1])
    user = User.objects.filter(groups__name='influencer')
    if user:
        payload = jwt_payload_handler(user[0])
        token = jwt_encode_handler(payload)
        # print('printing out token in cron job ', token)
        process_under_review_campaigns(token)
    else:
        print('Error: Unable to find the user.')

def scheduled_trouble_ticket_job():
    print("Trouble Ticket Cron called!", str(datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')))
    close_trouble_ticket()

    #cron for maturing the payment and send it to available balance from current earnings
    mature_payment_after_net_days()

def scheduled_paypal_job():
    print("Paypal Cron called!",
          str(datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')))
    paypal_batch_payout()
