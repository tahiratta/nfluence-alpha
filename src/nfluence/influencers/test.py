# s = {
#     "data": [
#         {
#             "id": "457850454",
#             "login": "emkay992",
#             "display_name": "emkay992",
#             "type": "",
#             "broadcaster_type": "",
#             "description": "",
#             "profile_image_url": "https://static-cdn.jtvnw.net/user-default-pictures-uv/de130ab0-def7-11e9-b668-784f43822e80-profile_image-300x300.png",
#             "offline_image_url": "",
#             "view_count": 0
#         }
#     ]
# }
# y = {
#     "total": 0,
#     "data": [],
#     "pagination": {}
# }
# # print(s['data'][0]['id'])
# # print(y['total'])
# import requests
# import json
#
# videos_url = "https://api.twitch.tv/helix/videos"
# videos_payload = {'user_id': 91103221}
# headers = {'content-type': 'application/x-www-form-urlencoded', 'Client-ID': 'nw96o83f1vvynmqra6k05drbpyjp5n'}
# req = requests.get(videos_url, params=videos_payload, headers=headers).text
# data = json.loads(req)
#
# # print(data['data'])
# total_count = 0
# total_videos = 0
# for elem in data['data']:
#     print(elem['view_count'])
#     total_count += int(elem['view_count'])
#     total_videos += 1
#
# print(total_count)
# print(total_videos)
# print("Average count: {0}".format(int(total_count / total_videos)))

# Testing BeautifulSoup
#
# from bs4 import BeautifulSoup
# from urllib.request import Request, urlopen
# import re
#
# req = Request('https://www.twitch.tv/kashif_cassandra/', headers={'User-Agent': 'Mozilla/5.0'})
# html_page = urlopen(req).read()
#
# soup = BeautifulSoup(html_page, 'html.parser')
# college_name = []
# div_selectors = soup.find_all('div')
# print(div_selectors)

# from PIL import Image
# import urllib.request
# from bs4 import BeautifulSoup
# from selenium import webdriver
# from selenium.webdriver.support.ui import WebDriverWait
# from selenium.webdriver.support import expected_conditions as EC
# from selenium.webdriver.common.by import By
# from selenium.common.exceptions import TimeoutException
# from selenium.webdriver.chrome.options import Options
# from selenium.webdriver import DesiredCapabilities
# options = webdriver.ChromeOptions()
# options = Options()
# options.add_argument('--headless')
# options.add_argument('--no-sandbox')
# options.headless = True

# options = Options()
# options.add_argument("--headless") # Runs Chrome in headless mode.
# options.add_argument('--no-sandbox') # # Bypass OS security model
# options.add_argument('start-maximized')
# options.add_argument('disable-infobars')
# options.add_argument("--disable-extensions")
# options.headless = True

#
# chrome_options.add_argument("--headless")
# chrome_options.add_argument('--lang=en_US')
# capabilities = DesiredCapabilities.CHROME.copy()
# capabilities['acceptSslCerts'] = True
# capabilities['acceptInsecureCerts'] = True
# driver = webdriver.Chrome(chrome_options=chrome_options, executable_path="/usr/lib/chromium-browser/chromedriver",
#                           desired_capabilities=capabilities)


# WORKING

# chrome_options = Options()
# chrome_options.add_argument('--headless')
# driver = webdriver.Chrome(chrome_options=chrome_options, executable_path="/usr/lib/chromium-browser/chromedriver")
# driver.set_window_size(1200, 600)
# driver.get('https://www.twitch.tv/emkay992')
# print("Headless Chrome Initialized on Linux OS")
# try:
#     WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.CLASS_NAME, 'channel-panels')))
# except TimeoutException:
#     print('Page timed out after 10 secs.')
# soup = BeautifulSoup(driver.page_source, 'html.parser')
# driver.quit()
# provided_url = 'www.intel.com'
# image_src = ''
# for elem in soup.findAll('a', attrs={'class': 'tw-interactive tw-link'}):
#     if provided_url in elem['href']:
#         print("FOUND {0}".format(elem['href']))
#         if elem.img:
#             print(elem.img['src'])
#             image_src = elem.img['src']
#
# print(image_src)
# if image_src:
#     with urllib.request.urlopen(image_src) as url:
#         with open('/home/munirkhan/nfluence/t_intel.jpg', 'wb') as f:
#             f.write(url.read())
#
#     img = Image.open('/home/munirkhan/nfluence/t_intel.jpg')
#
#     img.show()
#
# import cv2
#
# im = cv2.imread("/home/munirkhan/nfluence/t_intel.jpg")
# print("Twitch image shape: {}".format(im.shape))
# im_2 = cv2.imread("/home/munirkhan/nfluence/intel.png")
# print("original image shape: {}".format(im_2.shape))
#
# # Image hashing
#
# from PIL import Image
# import imagehash
#
# hash0 = imagehash.average_hash(Image.open('/home/munirkhan/nfluence/t_intel.jpg'))
# hash1 = imagehash.average_hash(Image.open('/home/munirkhan/nfluence/intel.png'))
# cutoff = 5
#
# if hash0 - hash1 < cutoff:
#     print('images are similar')
# else:
#     print('images are not similar')
#
# # ORB Technique
# import numpy as np
#
# def orb_test(img_1, img_2):
#     good = []
#     orb = cv2.ORB_create()
#     gray1 = cv2.cvtColor(img_1, cv2.COLOR_BGR2GRAY)
#     gray2 = cv2.cvtColor(img_2, cv2.COLOR_BGR2GRAY)
#
#     kpts1, descs1 = orb.detectAndCompute(gray1, None)
#     kpts2, descs2 = orb.detectAndCompute(gray2, None)
#
#     print(len(kpts1))
#     print(len(kpts2))
#
#     bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=False)
#
#     if descs2 is not None:
#         matches = bf.knnMatch(np.asarray(descs1, np.uint8), np.asarray(descs2, np.uint8), k=2)
#         for i, pair in enumerate(matches):
#             try:
#                 m, n = pair
#                 if m.distance < 0.5 * n.distance:
#                     good.append(m)
#
#             except ValueError:
#                 pass
#     return len(good)
#
# res = orb_test(im, im_2)
# print(res)


# GOOGLE VISION-API

# from google.cloud import vision
#
# import os
# import io
# #Google Credential Path
# #credential_path = r"read.json"
# credential_path = "/home/munirkhan/nfluence/nfluence-alpha-ml/Audio/nf-dev-01-1508eb164a02.json"
# os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = credential_path
#
#
# def detect_logos(path):
#     """Detects logos in the file."""
#     client = vision.ImageAnnotatorClient()
#     logos_list = []
#
#     with io.open(path, 'rb') as image_file:
#         content = image_file.read()
#     #content = path
#
#     image = vision.types.Image(content=content)
#
#     response = client.logo_detection(image=image)
#     logos = response.logo_annotations
#     #print('Logos:')
#
#     for logo in logos:
#         logos_list.append(logo.description)
#         #print(logo.description)
#     return logos_list
#
# t = detect_logos('/home/munirkhan/nfluence/t_intel.jpg')
#
# for x in t:
#     print(x)

# import sentry_sdk
# sentry_sdk.init("https://61fc68d3194845088ffd382fca3cafad@sentry.io/1875779")
#
# print(1 / 0)

from django.conf import settings
import requests
import json

def get_twitch_video_id(clip_url):
    clip_id = clip_url.split('/')[-1]
    twitch_clip_url = "https://api.twitch.tv/helix/clips?id=" + clip_id
    # followers_payload = {'to_id': user_id}
    headers = {'content-type': 'application/x-www-form-urlencoded', 'Client-ID': 'nw96o83f1vvynmqra6k05drbpyjp5n'}
    # headers = {'content-type': 'application/x-www-form-urlencoded', 'Client-ID': settings.TWITCH_CLIENT_ID}
    req = requests.get(twitch_clip_url, headers=headers).text
    data = json.loads(req)
    if data['data']:
        video_id = data['data'][0]['video_id']
        if video_id:
            return video_id
    else:
        return None
# video_id = get_twitch_video_id('https://clips.twitch.tv/FrigidHonestTitanResidentSleeper')
video_id = get_twitch_video_id('https://www.twitch.tv/emkay992')

print(video_id)

def get_twitch_video_details(video_id):
    twitch_clip_url = "https://api.twitch.tv/helix/videos?id=" + video_id
    headers = {'content-type': 'application/x-www-form-urlencoded', 'Client-ID': 'nw96o83f1vvynmqra6k05drbpyjp5n'}
    req = requests.get(twitch_clip_url, headers=headers).text
    data = json.loads(req)
    created_at = data['data'][0]['created_at']
    duration = data['data'][0]['duration']
    return created_at, duration

created_at, duration = get_twitch_video_details('515920617')
duration = duration.strip('s')
from datetime import datetime, timedelta

d2 = datetime.strptime(created_at, "%Y-%m-%dT%H:%M:%SZ")

# print(d1)
# print(d2)
#
# print(d2 - timedelta(seconds=int(duration)))
