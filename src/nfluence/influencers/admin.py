from django.contrib import admin
from influencers.models import (
    InfluencerCampaign, InfluencerCampaignSubmissionAudioConversions,
    InfluencerTwitchStat, InfluencerTwitchVideosStat, InfluencerCampaignSubmission,
    InfluencerCampaignSubmissionOverlay, InfluencerCampaignSubmissionOverlayFrames, TwitchDeltaViewCount,
    SubmitTicket, SubmitTicketImage
)

@admin.register(InfluencerCampaign)
class BrandAdmin(admin.ModelAdmin):

    def campaign_name(self, obj):
        return obj.campaign.name

    list_display = ('action', 'status', 'influencer_user_profile', 'campaign_name')
    # add filtering by yearly_budget
    list_filter = ('status',)
    # add search fields
    search_fields = ['action', 'status']

admin.site.register(InfluencerCampaignSubmissionAudioConversions)
admin.site.register(InfluencerCampaignSubmission)
admin.site.register(InfluencerCampaignSubmissionOverlayFrames)
admin.site.register(InfluencerCampaignSubmissionOverlay)
admin.site.register(InfluencerTwitchStat)
admin.site.register(InfluencerTwitchVideosStat)
admin.site.register(TwitchDeltaViewCount)
admin.site.register(SubmitTicket)
admin.site.register(SubmitTicketImage)
