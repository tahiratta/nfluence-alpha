import random
from rest_framework import serializers
from campaigns.models import (
    Campaign
)

from campaigns.serializers import (
    CampaignSerializer
)
from accounts.models import (
    UserProfile,
    InfluencerUserProfile
)

from influencers.models import (
    InfluencerCampaign,
    InfluencerCampaignSubmissionAudioConversions,
    InfluencerCampaignSubmission,
    InfluencerCampaignSubmissionOverlay,
    InfluencerCampaignSubmissionOverlayFrames,
    SubmitTicket,
    SubmitTicketImage,
)
from payments.models import (
    WithdrawAmount,
    # AvailableBalance
)
from accounts.serializers import UserSerializer
from django.contrib.auth import get_user_model

User = get_user_model()

from django.core.mail import EmailMessage
from datetime import datetime
from nfluence import settings


class SubmittedCampaignSerializer(serializers.ModelSerializer):
    name = serializers.CharField()
    type = serializers.CharField()
    status = serializers.CharField()
    campaign_brand_name = serializers.CharField()
    paid_out = serializers.CharField()
    campaign_budget = serializers.CharField()
    campaign_script = serializers.CharField()
    description = serializers.CharField()
    followers_count = serializers.CharField()
    logo_position = serializers.CharField()
    campaign_logo = serializers.CharField()
    num_of_days = serializers.CharField()
    campaign_underlay_url = serializers.CharField()
    total_influencers = serializers.CharField()

    class Meta:
        model = InfluencerCampaignSubmissionAudioConversions

        fields = (
            'id', 'name', 'type', 'campaign_brand_name', 'status', 'paid_out', 'total_influencers', 'campaign_budget',
            'campaign_script', 'description',
            'followers_count', 'logo_position', 'campaign_logo', 'num_of_days', 'campaign_underlay_url'
            # 'user'
        )


class InfluencerSerializer(serializers.ModelSerializer):
    # user = serializers.SerializerMethodField()
    #
    # @staticmethod
    # def get_user(obj):
    #     profile = obj.influencer_user_profile.user_profile
    #     user = User.objects.get(profile=profile)
    #     return user

    campaign = CampaignSerializer(required=False)

    class Meta:
        model = InfluencerCampaign
        fields = ('id', 'action', 'status', 'influencer_user_profile', 'campaign',
                  # 'user'
                  )
        # read_only_fields = ('user', )


class InfluencerCampaignSerializer(serializers.ModelSerializer):
    campaign_brand_name = serializers.SerializerMethodField()
    video_url = serializers.CharField(required=False)
    comments = serializers.CharField(required=False)
    uploaded_video_url = serializers.CharField(required=False)
    # estimated_payout = serializers.SerializerMethodField()
    influencer_status = serializers.CharField(required=False)
    influencer_status = serializers.SerializerMethodField(required=False)
    ticket_submitted_status = serializers.CharField(required=False)
    estimated_payout = serializers.FloatField(required=False)
    celery_task_id = serializers.CharField(required=False)

    @staticmethod
    def get_campaign_brand_name(obj):
        return obj.brand.name

    def get_influencer_status(self, obj):
        user_profile = self.context['request'].GET.get('user_profile', None)
        if user_profile:
            influencer_user_profile = InfluencerUserProfile.objects.get(
                user_profile=user_profile)
        else:
            influencer_user_profile = InfluencerUserProfile.objects.get(user_profile=self.context['request'].user.profile)

        ic = InfluencerCampaign.objects.filter(influencer_user_profile=influencer_user_profile,
                                               campaign=obj).order_by('-id')
        if ic.count() == 0:
            influencer_status = None
        else:
            influencer_status = ic[0].status

        return influencer_status

    # @staticmethod
    # def get_video_url(obj):
    #     return obj.InfluencerCampaignSubmission.video_url

    class Meta:
        model = Campaign
        # depth = 2
        fields = (
            'id', 'name', 'campaign_budget', 'campaign_logo', 'num_of_days', 'logo_position', 'video_url', 'type',
            'expiry',
            'start_date', 'end_date',
            'remaining_budget', 'description', 'created_on', 'updated_on', 'status', 'brand',
            'campaign_brand_name', 'influencer_status', 'ticket_submitted_status', 'estimated_payout', 'followers_count', 'comments',
            'uploaded_video_url', 'campaign_script', 'celery_task_id', 'campaign_underlay_url')
        read_only_fields = (
            'influencer_status', 'estimated_payout', 'video_url', 'comments', 'uploaded_video_url', 'celery_task_id',
            'campaign_underlay_url')


class InfluencerCampaignSubmissionSerializer(serializers.ModelSerializer):
    class Meta:
        model = InfluencerCampaignSubmission
        fields = (
            'id', 'video_url', 'comments', 'uploaded_video', 'influencer_user_profile', 'campaign', 'celery_task_id')


class InfluencerCampaignSubmissionAudioConversionsSerializer(serializers.ModelSerializer):
    # submission = InfluencerCampaignSubmissionSerializer(required=False)

    class Meta:
        model = InfluencerCampaignSubmissionAudioConversions
        fields = (
            'submission', 'campaign_script', 'speech_to_text', 'joint_words_script', 'short_script',
            'short_script_result',
            'soundex_text_result', 'soundex_text')

    def create(self, validated_data):
        # submission = validated_data.get('submission', None)
        # if submission:
        #     submission_data = validated_data.pop('submission')
        #     submission = InfluencerCampaignSubmission.objects.create(**submission_data)
        #     audio_conversion = InfluencerCampaignSubmissionAudioConversions(**validated_data, submission=submission)
        # else:
        audio_conversion = InfluencerCampaignSubmissionAudioConversions(**validated_data)
        audio_conversion.save()
        return audio_conversion


# class InfluencerCampaignSubmissionOverlaySerializer(serializers.ModelSerializer):
#     class Meta:
#         model = InfluencerCampaignSubmissionOverlay
#         fields = (
#             'submission', 'dl_logo_probability', 'predicted_orb_grid_pos', 'campaign_result')
#
#     def create(self, validated_data):
#         influencer_campaign_submission_overlay = InfluencerCampaignSubmissionOverlay(**validated_data)
#         influencer_campaign_submission_overlay.save()
#         return influencer_campaign_submission_overlay
#
# from rest_framework.response import Response
# class InfluencerCampaignSubmissionOverlayFramesSerializer(serializers.ModelSerializer):
#     uploaded_frame = serializers.JSONField()
#
#     class Meta:
#         model = InfluencerCampaignSubmissionOverlayFrames
#         fields = (
#             'submission', 'uploaded_frame')

# def create(self, validated_data):
#     # influencer_campaign_submission_overlay_frames = list(map(lambda uploaded_frame: InfluencerCampaignSubmissionOverlayFrames.objects.create(
#     #         submission=validated_data['submission'], uploaded_frame=uploaded_frame
#     #     ), validated_data['uploaded_frame']))
#     # for indx, uploaded_frame in enumerate(validated_data['uploaded_frame']):
#     #     influencer_campaign_submission_overlay_frames = InfluencerCampaignSubmissionOverlayFrames.objects.create(
#     #         submission=validated_data['submission'], uploaded_frame=uploaded_frame
#     #     )
#     print('influencer campaign submission', validated_data['submission'].id)
#     submission = InfluencerCampaignSubmission.objects.get(id=validated_data['submission'].id)
#     print('here is submission', submission)
#     for frame in validated_data['uploaded_frame']:
#         uploaded_frame = InfluencerCampaignSubmissionOverlayFrames.objects.create(
#                 submission=submission, uploaded_frame=frame
#             )
#     print('frmae', frame)
#     return frame
#     # print('dict order00', influencer_campaign_submission_overlay_frames)
#     # test = [object for object in influencer_campaign_submission_overlay_frames]
#     # print('tets sg as cw C', test)
#
#     # return influencer_campaign_submission_overlay_frames
#         # influencer_campaign_submission_overlay_frames.save()
#     # influencer_campaign_submission_overlay_frames = InfluencerCampaignSubmissionOverlayFrames(**validated_data)
#     # influencer_campaign_submission_overlay_frames.save()
#     # return influencer_campaign_submission_overlay_frames

class InfluencerCampaignSubmissionOverlaySerializer(serializers.ModelSerializer):
    class Meta:
        model = InfluencerCampaignSubmissionOverlay
        fields = (
            'submission', 'total_no_of_detected_logo_frames', 'no_of_dl_detected_logo_frames',
            'total_probability_of_detected_logo',
            'no_of_ORB_detected_logo_frames', 'campaign_result')
        read_only = (
        'total_no_of_detected_logo_frames', 'no_of_dl_detected_logo_frames', 'total_probability_of_detected_logo',
        'no_of_ORB_detected_logo_frames', 'campaign_result')


# def create(self, validated_data):
#     influencer_campaign_submission_overlay = InfluencerCampaignSubmissionOverlay(**validated_data)
#     influencer_campaign_submission_overlay.save()
#     return influencer_campaign_submission_overlay


class InfluencerCampaignSubmissionOverlayFramesSerializer(serializers.ModelSerializer):
    uploaded_frame = serializers.JSONField()
    orb_uploaded_frame = serializers.JSONField()
    dl_uploaded_frame = serializers.JSONField()
    # submission = serializers.CharField()
    # dl_logo_probability = serializers.CharField()
    # predicted_orb_grid_pos = serializers.CharField()
    # campaign_result = serializers.CharField()
    influencer_campaign_submission = InfluencerCampaignSubmissionOverlaySerializer(required=False)

    class Meta:
        model = InfluencerCampaignSubmissionOverlayFrames
        fields = ('uploaded_frame', 'orb_uploaded_frame', 'dl_uploaded_frame',
                  'influencer_campaign_submission')  # 'submission', 'dl_logo_probability', 'predicted_orb_grid_pos', 'campaign_result'


class InfluencerCampaignHistorySerializer(serializers.ModelSerializer):
    brand_id = serializers.SerializerMethodField()
    brand_name = serializers.SerializerMethodField()
    name = serializers.SerializerMethodField()
    type = serializers.SerializerMethodField()
    description = serializers.SerializerMethodField()
    # campaign = serializers.CharField()
    status = serializers.CharField()
    earned_amount = serializers.CharField()
    created_on = serializers.CharField()
   
    @staticmethod
    def get_brand_id(obj):
        return obj.campaign.brand_id

    @staticmethod
    def get_brand_name(obj):
        return obj.campaign.brand.name

    @staticmethod
    def get_name(obj):
        return obj.campaign.name

    @staticmethod
    def get_type(obj):
        return obj.campaign.type

    @staticmethod
    def get_description(obj):
        return obj.campaign.description

    class Meta:
        model = InfluencerCampaign
        fields = ('brand_id', 'brand_name', 'name', 'type', 'description', 'status', 'earned_amount', 'created_on',)


class SubmitTicketImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = SubmitTicketImage
        fields = ('attachment',)
from django.core import serializers as ser
import json

class SubmitTicketSerializer(serializers.ModelSerializer):
    attachments = SubmitTicketImageSerializer(source='submitticketimage_set', many=True, read_only=True)
    # name = serializers.SerializerMethodField()

    # def get_name(self, obj):
    #     if obj.campaign:
    #         data = ser.serialize('json', [obj.campaign, ])
    #         struct = json.loads(data)
    #         data = json.dumps(struct[0])
    #         return data
    #     else:
    #         return None

    class Meta:
        model = SubmitTicket
        fields = (
        'id','ticket_category', 'ticket_status', 'description_of_problem', 'influencer_email', 'influencer_name', 'campaign', 'attachments',
        'submission_date', 'observation_date', 'observation_time', 'user_profile', 'updated_on')

    def create(self, validated_data):
        images_data = self.context.get('view').request.FILES
        ticket_category = validated_data.get('ticket_category', "campaign")
        influencer_email = validated_data.get('influencer_email', "")
        influencer_name = validated_data.get('influencer_name', "")
        description_of_problem = validated_data.get('description_of_problem', "")
        campaign = validated_data.get('campaign', None)
        submission_date = validated_data.get('submission_date')
        observation_date = validated_data.get('observation_date', "")
        observation_time = validated_data.get('observation_time', "")
        user_profile = validated_data.get('user_profile')

        submit_ticket = SubmitTicket.objects.create(ticket_category=ticket_category,
                                                    description_of_problem=description_of_problem,
                                                    influencer_email=influencer_email, influencer_name=influencer_name,
                                                    campaign=campaign, submission_date=submission_date, observation_date=observation_date,
                                                    observation_time=observation_time, user_profile=user_profile)
        attachments_list = []
        for image_data in images_data.values():
            submit_ticket_image = SubmitTicketImage.objects.create(submit_ticket=submit_ticket, attachment=image_data)
            attachments_list.append(submit_ticket_image.attachment)

        status = ""
        if campaign:
            if user_profile:
                ic = InfluencerCampaign.objects.filter(influencer_user_profile__user_profile_id=user_profile,
                                                       campaign=campaign).order_by('-id')
                if ic:
                    status = ic[0].status

        subject = 'Trouble Ticket'
        if ticket_category == "campaign":
            body = 'Hi, Here are the details of ticket:\n\n' + 'Ticket Category: ' + ticket_category + '\n' + 'Campaign Name: ' + submit_ticket.campaign.name + '\n' \
                   +'Brand Name: ' + submit_ticket.campaign.brand.name + '\n'\
                   +'Campaign Type: ' + submit_ticket.campaign.type + '\n'\
                   +'Campaign Status: ' + status + '\n'\
                   +'Influencer Name: ' + influencer_name + '\n'\
                   +'Influencer Email: ' + influencer_email + '\n' \
                   + 'Description of Problem: ' + description_of_problem + '\n'\
                   +'Submission Date: ' + str(submission_date) + '\n'\
                   +'Observation Date: ' + str(observation_date) + '\n'\
                   +'Observation Time: ' + str(observation_time) + '\n'
            email_to = [influencer_email, 'TTCAMPAIGN992@gmail.com']

        else:
            body = 'Hi, Here are the details of ticket:\n\n' + 'Ticket Category: ' + ticket_category + '\n' \
                   + 'Influencer Name: ' + influencer_name + '\n' \
                   + 'Influencer Email: ' + influencer_email + '\n' \
                   + 'Description of Problem: ' + description_of_problem + '\n' \
                   + 'Submission Date: ' + str(submission_date) + '\n' \
                   + 'Observation Date: ' + str(observation_date) + '\n' \
                   + 'Observation Time: ' + str(observation_time) + '\n'
            email_to = [influencer_email, 'TTDEV992@gmail.com']

        email_from = 'mobeenarchi@gmail.com'

        msg = EmailMessage(subject, body,
                           email_from, email_to)
        # msg.content_subtype = "html"
        for file_to_attach in attachments_list:
            msg.attach_file(file_to_attach.path)
        msg.send()
        return submit_ticket

class CompanySubmitTicketSerializer(serializers.ModelSerializer):
    attachments = SubmitTicketImageSerializer(source='submitticketimage_set', many=True, read_only=True)
    campaign_name = serializers.CharField(required=False)

    class Meta:
        model = SubmitTicket
        fields = (
        'id','ticket_category', 'ticket_status', 'description_of_problem', 'influencer_email', 'influencer_name', 'campaign', 'campaign_name', 'attachments',
        'submission_date', 'observation_date', 'observation_time', 'user_profile', 'updated_on', 'updated_by')

class WithdrawAmountSerializer(serializers.ModelSerializer):
    total_earned = serializers.CharField(read_only=True)
    # total_withdrawal = serializers.SerializerMethodField(read_only=True)
    # current_earnings = serializers.SerializerMethodField(read_only=True)

    @staticmethod
    def get_total_earned(obj):
        return obj.available_balance


    class Meta:
        model = WithdrawAmount
        fields = ('withdraw_amount', 'user_profile', 'available_balance', 'total_earned')#, 'total_withdrawal', 'current_earnings')

    def create(self, validated_data):
        user_profile = validated_data.get('user_profile')
        withdraw_amount = validated_data.get('withdraw_amount')
        if withdraw_amount:
            queryset = WithdrawAmount.objects.filter(user_profile=user_profile).order_by('-created_on')
            if queryset:
                if withdraw_amount < queryset[0].available_balance:
                    try:
                        influencer_user_profile = InfluencerUserProfile.objects.get(user_profile=user_profile)
                    except InfluencerUserProfile.DoesNotExist:
                        raise serializers.ValidationError('Influencer User Profile Does not exist.')
                    total_withdrawl = 0
                    for amount in queryset:
                        total_withdrawl += amount.withdraw_amount
                    if influencer_user_profile.w9_form_submitted is False and total_withdrawl + withdraw_amount <= 590.00 or influencer_user_profile.w9_form_submitted is True:
                        if total_withdrawl >= 550.00 and influencer_user_profile.w9_form_submitted is False:
                            subject = "Submit W-9 Form"
                            body = "Hi " + influencer_user_profile.user_profile.user.username + ",\n\n" \
                                   + "Your withdrawl amount is going to approach $590, " \
                                     "To withdraw money over $590, Please emial w-9 form to 1099@nfluence.gg \n\n" \
                                   + "Thank you"
                            email_from = settings.EMAIL_HOST_USER
                            email_to = [influencer_user_profile.user_profile.email]

                            msg = EmailMessage(subject, body,
                                               email_from, email_to)
                            msg.send()
                        available_balance = queryset[0].available_balance - withdraw_amount
                        withdraw = WithdrawAmount.objects.create(user_profile=user_profile, withdraw_amount=withdraw_amount,
                                                                 available_balance=available_balance)
                    else:
                        raise serializers.ValidationError('Withdrawal Denied. Please submit W9-Form at 1099@nfluence.gg.')
                else:
                    raise serializers.ValidationError('You just entered an amount greater than your existing balance.')

                return withdraw
            else:
                raise serializers.ValidationError('You do not have enough balance to withdraw.')
        else:
            raise serializers.ValidationError('Please insert a valid amount.')

# class AvailableBalanceSerializer(serializers.ModelSerializer):
#     total_earned = serializers.SerializerMethodField(read_only=True)
#     total_withdrawal = serializers.SerializerMethodField(read_only=True)
#     current_earnings = serializers.SerializerMethodField(read_only=True)
#     class Meta:
#         model = AvailableBalance
#         fields = ('available_balance', 'influencer_user_profile',)
