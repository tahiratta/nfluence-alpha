from rest_framework.response import Response
from rest_framework import status
# import datetime
import requests
import logging
import os
import subprocess
import time
import random
import json
import decimal

from influencers.models import (
    InfluencerCampaign,
    InfluencerCampaignSubmission,
    InfluencerTwitchStat,
    InfluencerTwitchVideosStat,
    TwitchDeltaViewCount,
    SubmitTicket
)
from accounts.models import (
    InfluencerUserProfile,
    User
)
from payments.models import (
    Escrow,
    WithdrawAmount,
    PayoutItems
)
from campaigns.models import (
    Campaign
)
from campaigns.utils import (
    get_average_view_count,
    get_twitch_user_id
)
from nfluence import settings
from datetime import date
from django.db.models import Q
from datetime import datetime, timedelta
from campaigns.utils import (
    get_twitch_video_id,
    get_twitch_video_details
)
from django.utils import timezone
from django.core.paginator import Paginator
# from influencers.tasks import celery_backend

LOG = logging.getLogger('nfluence.influencers.views')

DOMAIN = os.environ.get('NFLUENCE_HOST_DOMAIN', 'http://192.168.101.139:8000')

# DOMAIN = os.environ.get('NFLUENCE_HOST_DOMAIN', 'http://nginx')
UNDERLAY_CAMPAIGN_PAYOUT = os.environ.get('UNDERLAY_CAMPAIGN_PAYOUT', 0.01)
OVERLAY_CAMPAIGN_PAYOUT = os.environ.get('OVERLAY_CAMPAIGN_PAYOUT', 0.03)
AUDIO_CAMPAIGN_PAYOUT = os.environ.get('AUDIO_CAMPAIGN_PAYOUT', 0.10)
NET_DAYS_MATURE_PAYMENT = os.environ.get('NET_DAYS_MATURE_PAYMENT', 30)


def get_suffix(day):
    if 4 <= day <= 20 or 24 <= day <= 30:
        suffix = "th"
    else:
        suffix = ["st", "nd", "rd"][day % 10 - 1]

    return suffix

def update_escrow(processed_video, campaign, influencer_campaign_submission, influencer_user_profile):
    if processed_video == "True" and campaign.type == "overlay":
        # Get the start_view_count and end_view_count of the influencer
        # delta = diff(end_view_count, start_view_count)
        # payout = delta * decimal.Decimal(UNDERLAY_CAMPAIGN_PAYOUT)
        # Update escrow table with the amount

        # Get Video id from the submitted clip_url
        delta = 1
        if influencer_campaign_submission.video_url:
            st_view_count = 1
            et_view_count = 1
            et_date, st_date = datetime.today(), datetime.today()
            video_id = get_twitch_video_id(influencer_campaign_submission.video_url)
            if video_id:
                created_at, duration = get_twitch_video_details(video_id)
                video_end_time = datetime.strptime(created_at, "%Y-%m-%dT%H:%M:%SZ")
                # duration = duration.strip('s')

                # Replace all the occurrences of string in list by some other values in the main list
                duration = replaceMultiple(duration, ['h', 'm', 's'])
                duration = eval(duration)
                # TODO make sure duration retunred from twitch api should be in seconds.
                video_start_time = video_end_time - timedelta(seconds=int(duration))

                # TODO need to check here stats are against one particular video or all the videos
                influencer_twitch_stats = InfluencerTwitchStat.objects.filter(
                    created_on__lte=video_start_time, influencer_user_profile=influencer_user_profile
                ).order_by(
                    '-id')

                if influencer_twitch_stats.count() > 0:
                    influencer_twitch_stats = influencer_twitch_stats[0]
                    if influencer_twitch_stats:
                        st_view_count = influencer_twitch_stats.view_count
                        st_view_count = 1 if st_view_count == 0 else st_view_count
                influencer_twitch_stats = InfluencerTwitchStat.objects.filter(
                    created_on__gte=video_end_time, influencer_user_profile=influencer_user_profile
                ).order_by(
                    'id')

                if influencer_twitch_stats.count() > 0:
                    influencer_twitch_stats = influencer_twitch_stats[0]
                    if influencer_twitch_stats:
                        et_view_count = influencer_twitch_stats.view_count
                        et_view_count = 1 if et_view_count == 0 else et_view_count

                delta = abs(et_view_count - st_view_count)
                delta = 1 if delta == 0 else delta

                TwitchDeltaViewCount.objects.create(delta=delta, created_on=datetime.today(),
                                                    influencer_user_profile=influencer_user_profile,
                                                    campaign_type=campaign.type)

                amount_estimated = calculate_avg_estimated_payout(influencer_user_profile, campaign, delta)
                #if escrow:
                #    escrow.save()

                # last_month_stream, last_two_months_stream,\
                # last_three_months_stream, number_of_days = get_all_streams_data(influencer_user_profile, campaign.type)
                # # last_three_months_stream, number_of_days = get_all_streams_data(delta, influencer_user_profile, campaign.type)
                #
                # # TODO check if days are 30, 60 or 90 and work accordingly
                #
                # # paginator1 = Paginator(last_month_stream, last_month_stream.count())
                # # paginator2 = Paginator(last_two_months_stream, last_two_months_stream.count())
                # # paginator3 = Paginator(last_three_months_stream, last_three_months_stream.count())
                # if number_of_days <= 30:
                #     last_month_stream_avg = one_month_avg(last_month_stream)
                #     print('avg1', last_month_stream_avg)
                # elif 30 < number_of_days <= 60:
                #     last_month_stream_avg, last_two_months_stream_avg = two_months_avg(last_month_stream, last_two_months_stream)
                #     print('avg1', last_month_stream_avg, 'avg2', last_two_months_stream_avg)
                # else:
                #     last_month_stream_avg, last_two_months_stream_avg,\
                #     last_three_months_stream_avg = three_months_avg(last_month_stream,
                #                                                     last_two_months_stream, last_three_months_stream)
                #     print('avg1', last_month_stream_avg, 'avg2', last_two_months_stream_avg, 'avg3', last_three_months_stream_avg)
                #
                # #TODO calculate estimated payout and save into escrow table in amount_estimated column
                #
                # escrow = Escrow.objects.filter(influencer_user_profile=influencer_user_profile,
                #                                campaign=campaign).order_by('-id')
                # if escrow.count() > 0:
                #     escrow = escrow[0]
                #     if escrow:
                #         escrow.amount_actual = delta * decimal.Decimal(OVERLAY_CAMPAIGN_PAYOUT)
                #         if number_of_days <= 30:
                #             amount_estimated = last_month_estimated_payout(last_month_stream_avg, campaign.type)
                #         elif 30 < number_of_days <= 60:
                #             amount_estimated = last_two_months_estimated_payout(last_month_stream_avg, last_two_months_stream_avg, campaign.type)
                #         else:
                #             amount_estimated = last_three_months_estimated_payout(last_month_stream_avg,
                #                                                                 last_two_months_stream_avg, last_three_months_stream_avg, campaign.type)
                #         escrow.amount_estimated = round(amount_estimated, 2)
                #         print('estimated_payout', escrow.amount_estimated)
                #     escrow.save()
    elif processed_video == "True" and campaign.type == "audio":
        if influencer_campaign_submission.video_url:
            video_id = get_twitch_video_id(influencer_campaign_submission.video_url)
            if video_id:
                created_at, duration = get_twitch_video_details(video_id)
                video_end_time = datetime.strptime(created_at, "%Y-%m-%dT%H:%M:%SZ")
                # duration = duration.strip('s')
                
                # Replace all the occurrences of string in list by some other values in the main list
                duration = replaceMultiple(duration, ['h', 'm', 's'])
                duration = eval(duration)
                video_start_time = video_end_time - timedelta(seconds=int(duration))

                influencer_twitch_stats = InfluencerTwitchStat.objects.filter(
                    created_on__gte=video_start_time, created_on__lte=video_end_time
                ).order_by(
                    'view_count')
                view_counts = []
                for stat in influencer_twitch_stats:
                    view_counts.append(stat.view_count)

                if len(view_counts) > 0:
                    view_counts = view_counts[1:]
                    delta = sum(view_counts) / len(view_counts)
                    delta = 1 if delta == 0 else delta

                    # concurrent_view_counts = influencer_twitch_stats[len(view_counts)].view_count

                    TwitchDeltaViewCount.objects.create(delta=delta, created_on=datetime.today(),
                                                        influencer_user_profile=influencer_user_profile,
                                                        campaign_type=campaign.type)

                    # last_month_stream, last_two_months_stream, \
                    # last_three_months_stream, number_of_days = get_all_streams_data(influencer_user_profile,
                    #                                                                 campaign.type)
                    #
                    # # last_month_stream, last_two_months_stream, \
                    # # last_three_months_stream, number_of_days = get_all_streams_data(delta, influencer_user_profile, campaign.type)
                    #
                    # if number_of_days <= 30:
                    #     last_month_stream_avg = one_month_avg(last_month_stream)
                    #     print('avg1', last_month_stream_avg)
                    # elif 30 < number_of_days <= 60:
                    #     last_month_stream_avg, last_two_months_stream_avg = two_months_avg(last_month_stream,
                    #                                                                        last_two_months_stream)
                    #     print('avg1', last_month_stream_avg, 'avg2', last_two_months_stream_avg)
                    # else:
                    #     last_month_stream_avg, last_two_months_stream_avg, \
                    #     last_three_months_stream_avg = three_months_avg(last_month_stream,
                    #                                                     last_two_months_stream,
                    #                                                     last_three_months_stream)
                    #     print('avg1', last_month_stream_avg, 'avg2', last_two_months_stream_avg, 'avg3',
                    #           last_three_months_stream_avg)
                    #
                    # escrow = Escrow.objects.filter(influencer_user_profile=influencer_user_profile,
                    #                                campaign=campaign).order_by('-id')
                    # if escrow.count() > 0:
                    #     escrow = escrow[0]
                    #     if escrow:
                    #         escrow.amount_actual = decimal.Decimal(delta * AUDIO_CAMPAIGN_PAYOUT)
                    #
                    #         #TODO chnage every month estimated payout according to audio campaign
                    #
                    #         if number_of_days <= 30:
                    #             amount_estimated = last_month_estimated_payout(last_month_stream_avg, campaign.type)
                    #         elif 30 < number_of_days <= 60:
                    #             amount_estimated = last_two_months_estimated_payout(last_month_stream_avg,
                    #                                                                 last_two_months_stream_avg,
                    #                                                                 campaign.type)
                    #         else:
                    #             amount_estimated = last_three_months_estimated_payout(last_month_stream_avg,
                    #                                                                   last_two_months_stream_avg,
                    #                                                                   last_three_months_stream_avg,
                    #                                                                   campaign.type)
                    #         escrow.amount_estimated = round(amount_estimated, 2)
                    #         print('estimated_payout', escrow.amount_estimated)
                    #     escrow.save()
                    amount_estimated = calculate_avg_estimated_payout(influencer_user_profile, campaign, delta)
                    #if escrow:
                    #    escrow.save()

#CALCULATE AVG ESTIMATED PAYOUT
def calculate_avg_estimated_payout(influencer_user_profile, campaign, delta):
    last_month_stream, last_two_months_stream, \
    last_three_months_stream, number_of_days = get_all_streams_data(influencer_user_profile, campaign.type)
    # last_three_months_stream, number_of_days = get_all_streams_data(delta, influencer_user_profile, campaign.type)

    # TODO check if days are 30, 60 or 90 and work accordingly

    # paginator1 = Paginator(last_month_stream, last_month_stream.count())
    # paginator2 = Paginator(last_two_months_stream, last_two_months_stream.count())
    # paginator3 = Paginator(last_three_months_stream, last_three_months_stream.count())
    if number_of_days <= 30:
        last_month_stream_avg = one_month_avg(last_month_stream)
        print('avg1', last_month_stream_avg)
    elif 30 < number_of_days <= 60:
        last_month_stream_avg, last_two_months_stream_avg = two_months_avg(last_month_stream,
                                                                           last_two_months_stream)
        print('avg1', last_month_stream_avg, 'avg2', last_two_months_stream_avg)
    else:
        last_month_stream_avg, last_two_months_stream_avg, \
        last_three_months_stream_avg = three_months_avg(last_month_stream,
                                                        last_two_months_stream, last_three_months_stream)
        print('avg1', last_month_stream_avg, 'avg2', last_two_months_stream_avg, 'avg3',
              last_three_months_stream_avg)

    # TODO calculate estimated payout and save into escrow table in amount_estimated column

    escrow = Escrow.objects.filter(influencer_user_profile=influencer_user_profile,
                                   campaign=campaign).order_by('-id')
    if escrow.count() > 0:
        escrow = escrow[0]
        if escrow:
            if campaign.type == "overlay":
                escrow.amount_actual = delta * decimal.Decimal(OVERLAY_CAMPAIGN_PAYOUT)
            elif campaign.type == "audio":
                escrow.amount_actual = delta * decimal.Decimal(AUDIO_CAMPAIGN_PAYOUT)
            else:
                escrow.amount_actual = delta * decimal.Decimal(UNDERLAY_CAMPAIGN_PAYOUT)
            if number_of_days <= 30:
                amount_estimated = last_month_estimated_payout(last_month_stream_avg, campaign.type)
            elif 30 < number_of_days <= 60:
                amount_estimated = last_two_months_estimated_payout(last_month_stream_avg,
                                                                    last_two_months_stream_avg,
                                                                    campaign.type)
            else:
                amount_estimated = last_three_months_estimated_payout(last_month_stream_avg,
                                                                      last_two_months_stream_avg,
                                                                      last_three_months_stream_avg,
                                                                      campaign.type)
            escrow.amount_estimated = round(amount_estimated, 2)
            print('estimated_payout', escrow.amount_estimated)
            escrow.save()
            return escrow.amount_estimated
        else:
            return None
    else:
        if campaign.type == "overlay":
            amount_actual = delta * decimal.Decimal(OVERLAY_CAMPAIGN_PAYOUT)
        else:
            amount_actual = delta * decimal.Decimal(AUDIO_CAMPAIGN_PAYOUT)
        if number_of_days <= 30:
            amount_estimated = last_month_estimated_payout(last_month_stream_avg, campaign.type)
        elif 30 < number_of_days <= 60:
            amount_estimated = last_two_months_estimated_payout(last_month_stream_avg,
                                                                last_two_months_stream_avg,
                                                                campaign.type)
        else:
            amount_estimated = last_three_months_estimated_payout(last_month_stream_avg,
                                                                  last_two_months_stream_avg,
                                                                  last_three_months_stream_avg,
                                                                campaign.type)
        amount_estimated = round(amount_estimated, 2)
        escrow = Escrow.objects.create(influencer_user_profile=influencer_user_profile,
                                       campaign=campaign, amount_actual=amount_actual, amount_estimated=amount_estimated)
        return escrow.amount_estimated

def convert_seconds_to_dhms(seconds):
    day = seconds // (24 * 3600)
    time = seconds % (24 * 3600)
    hour = time // 3600
    time %= 3600
    minutes = time // 60
    time %= 60
    seconds = time
    return "%d:%d:%d:%d" % (day, hour, minutes, seconds)

'''
Replace a set of multiple sub strings with a new string in main string.
'''

def replaceMultiple(mainString, toBeReplaces):
    hours_replacement = "*3600+"
    mins_replacement = "*60+"
    secs_replacement = ""
    # Iterate over the strings to be replaced
    for elem in toBeReplaces:

        # Check if string is in the main string
        if elem in mainString:
            if elem == 'h':
                # Replace the string
                mainString = mainString.replace(elem, hours_replacement)
            elif elem == 'm':
                # Replace the string
                mainString = mainString.replace(elem, mins_replacement)
            else:
                # Replace the string
                mainString = mainString.replace(elem, secs_replacement)

    return mainString

def avg_estimated_payout_for_all_influencers():
    influencer_user_profiles = InfluencerUserProfile.objects.all()
    campaigns = Campaign.objects.filter(status='active')

    for campaign in campaigns:
        for influencer_user_profile in influencer_user_profiles:
            delta = 1
            delta_view_count = TwitchDeltaViewCount.objects.filter(influencer_user_profile=influencer_user_profile,
                                                                   campaign_type=campaign.type).order_by('-id').first()
            if delta_view_count:
                delta = delta_view_count.delta
            amount_estimated = calculate_avg_estimated_payout(influencer_user_profile, campaign, delta)

def get_amount_estimated(influencer_user_profile, campaign_type, user_id):
    amount_estimated = 0
    if campaign_type == "underlay":
        average_days = 1
        amount_estimated = get_average_twitch_view_count_underlay(influencer_user_profile, average_days)
        if not amount_estimated:
            amount_estimated = decimal.Decimal(UNDERLAY_CAMPAIGN_PAYOUT)
    else:
        avg_view_count = get_average_view_count(settings.TWITCH_CLIENT_ID,
                                                user_id)
        if avg_view_count is not None:
            amount_estimated = avg_view_count * decimal.Decimal(OVERLAY_CAMPAIGN_PAYOUT)
        else:
            amount_estimated = decimal.Decimal(OVERLAY_CAMPAIGN_PAYOUT)
    return amount_estimated

def get_influencers_amount_estimated(influencer_user_profile, campaign_type):
    amount_estimated = 0
    if campaign_type == "underlay":
        average_days = 1
        amount_estimated = get_average_twitch_view_count_underlay(influencer_user_profile, average_days)
        if not amount_estimated:
            amount_estimated = decimal.Decimal(UNDERLAY_CAMPAIGN_PAYOUT)
    else:
        avg_view_count = InfluencerTwitchVideosStat.objects.filter(influencer_user_profile=influencer_user_profile).order_by('-created_on').first()
        if avg_view_count is not None:
            amount_estimated = avg_view_count.avg_count * decimal.Decimal(OVERLAY_CAMPAIGN_PAYOUT)
        else:
            amount_estimated = decimal.Decimal(OVERLAY_CAMPAIGN_PAYOUT)
    return amount_estimated

def get_average_twitch_view_count_underlay(influencer_user_profile, avg_days):
    influencer_twitch_stats = InfluencerTwitchStat.objects.filter(influencer_user_profile=influencer_user_profile,
                                                                  created_on__lt=datetime.today(),
                                                                  created_on__gt=datetime.today() - timedelta(
                                                                      days=avg_days))
    total_records = influencer_twitch_stats.count()
    total_view_count = 0

    if total_records > 0:
        for stat in influencer_twitch_stats:
            total_view_count += stat.view_count
        return decimal.Decimal(total_view_count / total_records) * decimal.Decimal(UNDERLAY_CAMPAIGN_PAYOUT)
    else:
        return decimal.Decimal(UNDERLAY_CAMPAIGN_PAYOUT)


#-------------------------------------------#

def submitted_under_review_campaigns_backend(campaign_id, influencer_campaign_submission_id, influencer_user_profile_id, auth_token):
    response = {}
    processed_video = None
    CAMPAIGN_ASSET_URLS = dict()
    try:
        campaign = Campaign.objects.get(id=campaign_id)
        influencer_campaign_submission = InfluencerCampaignSubmission.objects.get(id=influencer_campaign_submission_id)
        influencer_user_profile = InfluencerUserProfile.objects.get(id=influencer_user_profile_id)

#        if campaign.type == "overlay" or campaign.type == "audio":
#            # Convert the video to .avi
#            if influencer_campaign_submission.uploaded_video.path:
#                avi_file, time_str = video_audio_conversion(campaign.type,
#                                                            influencer_campaign_submission.uploaded_video.path,
#                                                            campaign.id)

        if campaign.type == "overlay":
            # If converted successfully, update the extension of the video
#            if avi_file:
#                influencer_campaign_submission.uploaded_video = avi_file
#                influencer_campaign_submission.save()


            #CAMPAIGN_ASSET_URLS['SUBMITTED_VIDEO_URL'] = DOMAIN + influencer_campaign_submission.uploaded_video.url
            CAMPAIGN_ASSET_URLS['SUBMITTED_VIDEO_URL'] = str(influencer_campaign_submission.uploaded_video.name)
            print('Submitted video url *****', CAMPAIGN_ASSET_URLS['SUBMITTED_VIDEO_URL'])
            #CAMPAIGN_ASSET_URLS[
            #    'SUBMITTED_CAMPAIGN_LOGO_URL'] = DOMAIN + influencer_campaign_submission.campaign.campaign_logo.url
            CAMPAIGN_ASSET_URLS['SUBMITTED_CAMPAIGN_LOGO_URL'] = str(influencer_campaign_submission.campaign.campaign_logo)
            print('Submittel logo url *****', CAMPAIGN_ASSET_URLS['SUBMITTED_CAMPAIGN_LOGO_URL'])
            # Calling the flask app for video processing
            processed_video = video_processing(campaign.brand.name,
                                               settings.FLASK_APP_ML_OVERLAY,
                                               CAMPAIGN_ASSET_URLS['SUBMITTED_VIDEO_URL'],
                                               CAMPAIGN_ASSET_URLS['SUBMITTED_CAMPAIGN_LOGO_URL'],
                                               influencer_campaign_submission.campaign.logo_position,
                                               campaign.id,
                                               campaign.brand.id,
                                               influencer_campaign_submission.influencer_user_profile.id,
                                               influencer_campaign_submission_id,
                                               auth_token)
        elif campaign.type == "audio":
#            if avi_file:
#                influencer_campaign_submission.uploaded_audio = avi_file
#                influencer_campaign_submission.save()
            #CAMPAIGN_ASSET_URLS['SUBMITTED_AUDIO_URL'] = DOMAIN + influencer_campaign_submission.uploaded_audio.url
            #CAMPAIGN_ASSET_URLS['SUBMITTED_AUDIO_URL'] = DOMAIN + influencer_campaign_submission.uploaded_video.url
            CAMPAIGN_ASSET_URLS['SUBMITTED_AUDIO_URL'] = str(influencer_campaign_submission.uploaded_video.name)
            print('submitted audio url', CAMPAIGN_ASSET_URLS['SUBMITTED_AUDIO_URL'])
            campaign_audio_file_text = campaign.campaign_audio_text_file
            if campaign.campaign_script:
                audio_campaign_text = campaign.campaign_script
            elif campaign_audio_file_text:
                audio_campaign_text = campaign_audio_file_text.readlines()
                x = [x.decode('utf-8') for x in audio_campaign_text]
                x_ = [elem.strip("\n") for elem in x]
                audio_campaign_text = ' '.join(x_)
            else:
                audio_campaign_text = "Audio not found!"

            # Calling the flask app for video processing
            processed_video = audio_processing(settings.FLASK_APP_ML_AUDIO,
                                               CAMPAIGN_ASSET_URLS['SUBMITTED_AUDIO_URL'],
                                               audio_campaign_text, influencer_campaign_submission_id, auth_token)

        elif campaign.type == "underlay":
            CAMPAIGN_ASSET_URLS['SUBMITTED_UNDERLAY_URL'] = influencer_campaign_submission.video_url
            CAMPAIGN_ASSET_URLS[
                #'SUBMITTED_UNDERLAY_IMAGE_URL'] = DOMAIN + influencer_campaign_submission.campaign.campaign_logo.url
                'SUBMITTED_UNDERLAY_IMAGE_URL'] = str(influencer_campaign_submission.campaign.campaign_logo)

            print('submitted underlay url @@@@@@@@@@@', CAMPAIGN_ASSET_URLS['SUBMITTED_UNDERLAY_URL'], CAMPAIGN_ASSET_URLS['SUBMITTED_UNDERLAY_IMAGE_URL'])
            processed_video = underlay_processing(
                settings.FLASK_APP_ML_UNDERLAY,
                CAMPAIGN_ASSET_URLS['SUBMITTED_UNDERLAY_URL'],
                campaign.campaign_underlay_url,
                CAMPAIGN_ASSET_URLS['SUBMITTED_UNDERLAY_IMAGE_URL'],
                campaign.brand.id,
                campaign.id,
                influencer_user_profile_id
            )

        influencer_campaign_submission.save()

        # Process the payment if machine learning test is passes
        # Change the payment status
        print('status for processed video ^^^^^^^', processed_video)
        if processed_video in ["True", "False", "Partial True"]:
            update_escrow(processed_video, campaign, influencer_campaign_submission, influencer_user_profile)
            processed_video_status(campaign, processed_video, influencer_user_profile, campaign.id)
        influencer_campaign = InfluencerCampaign.objects.filter(campaign=campaign,
                                                                influencer_user_profile=influencer_user_profile) \
            .order_by('-id')[0]
        response['status'] = influencer_campaign.status
        response['submission_id'] = influencer_campaign_submission.id
        print('this is the response ', response)
        return response
    except Exception as e: return e

#-------------------------------------------#



def process_under_review_campaigns(token):
    ''' Get all the influencercampaign with the following conds.
        type: any
        status: under_review
        time: greater than 2 minutes '''

    #from influencers.tasks import celery_backend
    #two_minutes = datetime.now() - timedelta(minutes=2)
    # campaign__type="underlay"
    #InfluencerCampaign.objects.filter(status__in=["under_review", "under_review_ul"]).delete()
    #return
    under_review_campaigns = InfluencerCampaign.objects.filter(status__in=["under_review", "under_review_ul"]) #.distinct('campaign_id')
                                                               #created_on__lt=two_minutes).distinct('campaign_id')
    #under_review_campaigns = InfluencerCampaign.objects.filter(status="under_review_ul")
    print('ucp', under_review_campaigns)
    for ur_cp in under_review_campaigns:
        print('campaign name ****************', ur_cp.campaign.name, ur_cp.campaign.type, ur_cp.campaign.status)
        ic_count = InfluencerCampaign.objects.filter(campaign_id=ur_cp.campaign_id, influencer_user_profile=ur_cp.influencer_user_profile).count()
        print(ic_count)
        if ic_count == 2:
            # Trigger Celery to force run the campaign
            influencer_campaign_submission_id = InfluencerCampaignSubmission.objects.filter(
                campaign_id=ur_cp.campaign_id).values('id')
            #result = celery_backend.apply(args=[ur_cp.campaign_id, influencer_campaign_submission_id[0]['id'],
            #                              ur_cp.influencer_user_profile_id, token])


            result = submitted_under_review_campaigns_backend(ur_cp.campaign_id, influencer_campaign_submission_id[0]['id'],
                                          ur_cp.influencer_user_profile_id, token)
            

            #response = {'status': True} #, 'task_id': result.id}
            print("response: ", result)


def close_trouble_ticket():
    datetime_before_week = datetime.now() - timedelta(days=7)
    tickets = SubmitTicket.objects.filter(created_on__lte=datetime_before_week)
    for ticket in tickets:
        if ticket.ticket_status != "Done Paid" and ticket.ticket_category == "campaign":
            try:
                influencer_user_profile = InfluencerUserProfile.objects.get(user_profile=ticket.user_profile)
            except InfluencerUserProfile.DoesNotExist:
                return Response({'status': False, 'error': 'profile_not_found'},
                                status=status.HTTP_500_INTERNAL_SERVER_ERROR)
            processed_video = "True"
            processed_video_status(ticket.campaign, processed_video, influencer_user_profile,
                                                    ticket.campaign.id)

            SubmitTicket.objects.filter(
                id=ticket.id).update(ticket_status='Done Paid', updated_on=datetime.now())

def mature_payment_after_net_days():
    datetime_before_net_days = datetime.now() - timedelta(days=NET_DAYS_MATURE_PAYMENT)
    escrows = Escrow.objects.filter(created_on__lte=datetime_before_net_days, transaction_status='paid', payment_mature_status="immatured")
    for escrow in escrows:
        withdraw_amount = WithdrawAmount.objects.filter(user_profile=escrow.influencer_user_profile.user_profile).order_by('-created_on')
        if withdraw_amount:
            available_balance = withdraw_amount[0].available_balance + escrow.amount_actual
            WithdrawAmount.objects.create(withdraw_amount=withdraw_amount[0].withdraw_amount, available_balance=available_balance,
                                          user_profile=escrow.influencer_user_profile.user_profile, campaign=escrow.campaign, matured_amount=escrow.amount_actual)
        else:
            available_balance = escrow.amount_actual
            WithdrawAmount.objects.create(available_balance=available_balance,
                                          user_profile=escrow.influencer_user_profile.user_profile,
                                          campaign=escrow.campaign, matured_amount=escrow.amount_actual)
        escrow.payment_mature_status = "matured"
        escrow.save()

def check_underlay_timeout():
    ''' Get all the influencercampaign with the following conds.
    type: underlay
    created_on_lt 72 hours
    status: accepted '''

    # seventy_two_hours = datetime.now() - datetime.timedelta(days=3)
    ten_minutes = datetime.now() - timedelta(minutes=10)
    # campaign__type="underlay"
    timed_out_campaigns = InfluencerCampaign.objects.filter(status="accepted",
                                                            created_on__lt=ten_minutes).distinct('campaign_id')

    for tc in timed_out_campaigns:
        ic_count = InfluencerCampaign.objects.filter(campaign_id=tc.campaign_id).count()
        if not ic_count > 1:
            InfluencerCampaign.objects.create(action="time_out", status="timed_out",
                                              influencer_user_profile=tc.influencer_user_profile,
                                              campaign_id=tc.campaign_id)
            # TODO Update the campaign remaining budget, i.e. add the escrow to remaining budget
            update_campaign_budget(tc.campaign, tc.influencer_user_profile, tc.campaign_id)


def get_underlay_result():
    influencer_list = list(InfluencerUserProfile.objects.all())
    CAMPAIGN_ASSET_URLS = dict()
    today = timezone.now()

    for influencer in influencer_list:
        iterated_campaign_ids = []
        # twelve_hours = datetime.now() - timedelta(seconds=10)
        four_minutes = datetime.now() - timedelta(minutes=4)
        campaigns = InfluencerCampaign.objects.filter(created_on__lt=four_minutes, influencer_user_profile=influencer,
                                                      campaign__type="underlay",
                                                      campaign__num_of_days__isnull=False).order_by('-id')
        for campaign in campaigns:
            if campaign.campaign_id not in iterated_campaign_ids:
                iterated_campaign_ids.append(campaign.campaign_id)
                try:
                    influencer_campaign_submission = InfluencerCampaignSubmission.objects.get(
                        influencer_user_profile=influencer,
                        campaign=campaign.campaign, payment_status="not_started")
                except InfluencerCampaignSubmission.DoesNotExist:
                    influencer_campaign_submission = None
                except InfluencerCampaignSubmission.MultipleObjectsReturned:
                    influencer_campaign_submission = None

                if influencer_campaign_submission:
                    # campaign_ = Campaign.objects.get(campaign.campaign_id)

                    # If campaign expired, update the statuses in the processed_video_status
                    # Check if the num_of_days have passed after the influencer_campaign entry
                    # add the num of days to inf_camp entry and check if the date is today
                    campaign_expiry = campaign.campaign.created_on + timedelta(days=campaign.campaign.num_of_days)
                    # today += timedelta(days=2)
                    if campaign_expiry <= today and campaign.status != "pending_payment":
                        # Get the aggregate of the InfluencerCampaign table and decide statuses
                        # get total passed status campaigns
                        passed_campaigns = InfluencerCampaign.objects.filter(influencer_user_profile=influencer.id,
                                                                             status="true",
                                                                             campaign=campaign.campaign.id
                                                                             ).count()
                        failed_campaigns = InfluencerCampaign.objects.filter(influencer_user_profile=influencer.id,
                                                                             status="false",
                                                                             campaign=campaign.campaign.id
                                                                             ).count()

                        if passed_campaigns >= 0:
                            result = passed_campaigns / (passed_campaigns + failed_campaigns) * 100 if passed_campaigns > 0 else 0
                            if result >= 60:
                                # Update the ai_status in the InfluencerCampaignSubmission table
                                InfluencerCampaignSubmission.objects.filter(influencer_user_profile=influencer.id,
                                                                            campaign_id=campaign.campaign.id).update(
                                    ai_status="passed",
                                    payment_status="pending_payment")
                                InfluencerCampaign.objects.create(action="submission_ul_completed",
                                                                  status="pending_payment",
                                                                  influencer_user_profile=influencer,
                                                                  campaign_id=campaign.campaign.id)

                                # Get the start_view_count and end_view_count of the influencer
                                # delta = diff(end_view_count, start_view_count)
                                # payout = delta * decimal.Decimal(UNDERLAY_CAMPAIGN_PAYOUT)
                                # Update escrow table with the amount
                                # Start date is the date influencer submitted the campaign
                                # End date is the duration of the campaign i.e. num_of_days
                                delta = get_influencer_twitch_view_count_underlay(influencer,
                                                                                  campaign.created_on.date(),
                                                                                  campaign_expiry.date())
                                TwitchDeltaViewCount.objects.create(delta=delta, created_on=datetime.today(), influencer_user_profile=influencer, campaign_type=campaign.campaign.type)

                                #last_month_stream, last_two_months_stream, \
                                #last_three_months_stream, number_of_days = get_all_streams_data(influencer, campaign.campaign.type)
                                #if number_of_days <= 30:
                                #    last_month_stream_avg = one_month_avg(last_month_stream)
                                #    print('avg1', last_month_stream_avg)
                                #elif 30 < number_of_days <= 60:
                                #    last_month_stream_avg, last_two_months_stream_avg = two_months_avg(
                                #        last_month_stream, last_two_months_stream)
                                #    print('avg1', last_month_stream_avg, 'avg2', last_two_months_stream_avg)
                                #else:
                                #    last_month_stream_avg, last_two_months_stream_avg, \
                                #    last_three_months_stream_avg = three_months_avg(last_month_stream,
                                #                                                    last_two_months_stream,
                                #                                                    last_three_months_stream)
                                #    print('avg1', last_month_stream_avg, 'avg2', last_two_months_stream_avg, 'avg3',
                                #          last_three_months_stream_avg)

                                #escrow = Escrow.objects.filter(influencer_user_profile=influencer,
                                #                               campaign=campaign.campaign).order_by('-id')[0]
                                #if escrow:
                                #    escrow.amount_actual = delta * decimal.Decimal(UNDERLAY_CAMPAIGN_PAYOUT)
                                #    if number_of_days <= 30:
                                #        amount_estimated = last_month_estimated_payout(last_month_stream_avg, campaign.campaign.type)
                                #    elif 30 < number_of_days <= 60:
                                #        amount_estimated = last_two_months_estimated_payout(last_month_stream_avg,
                                #                                                            last_two_months_stream_avg, campaign.campaign.type)
                                #    else:
                                #        amount_estimated = last_three_months_estimated_payout(last_month_stream_avg,
                                #                                                              last_two_months_stream_avg,
                                #                                                              last_three_months_stream_avg, campaign.campaign.type)
                                #    escrow.amount_estimated = round(amount_estimated, 2)
                                #    print('estimated_payout', escrow.amount_estimated)
                                #escrow.save()
                                calculate_avg_estimated_payout(influencer, campaign.campaign, delta)
                            else:
                                InfluencerCampaignSubmission.objects.filter(influencer_user_profile=influencer.id,
                                                                            campaign_id=campaign.campaign.id).update(
                                    ai_status="failed",
                                    payment_status="campaign_failed")
                                InfluencerCampaign.objects.create(action="submission_ul_completed",
                                                                  status="campaign_failed",
                                                                  influencer_user_profile=influencer,
                                                                  campaign_id=campaign.campaign.id)
                                # Update campaign budget
                                update_campaign_budget(campaign.campaign, influencer, campaign.campaign_id)
                        else:
                            print('cause passed camps are 0', influencer_campaign_submission)
                    else:
                        CAMPAIGN_ASSET_URLS['SUBMITTED_UNDERLAY_URL'] = influencer_campaign_submission.video_url
                        CAMPAIGN_ASSET_URLS[
                            #'SUBMITTED_UNDERLAY_IMAGE_URL'] = DOMAIN + influencer_campaign_submission.campaign.campaign_logo.url
                            'SUBMITTED_UNDERLAY_IMAGE_URL'] = influencer_campaign_submission.campaign.campaign_logo

                        processed_video = underlay_processing(
                            settings.FLASK_APP_ML_UNDERLAY,
                            CAMPAIGN_ASSET_URLS['SUBMITTED_UNDERLAY_URL'],
                            campaign.campaign.campaign_underlay_url,
                            CAMPAIGN_ASSET_URLS['SUBMITTED_UNDERLAY_IMAGE_URL'],
                            campaign.campaign.brand.id,
                            campaign.campaign.id,
                            influencer.id
                        )
                        if processed_video == "True" or processed_video == "False":
                            processed_video_status(campaign.campaign, processed_video, influencer, campaign.campaign.id)

# Functions to save delta, calculate avgs of deltas for different streams
# for one month, two months and three months as well as to calculate
# amount estimated payout.

#****************************************************************************************

def get_all_streams_data(influencer_user_profile, campaign_type):
    # TODO need to save delta for every stream into db with created_at date, campaign_type and influencer_profile
    #TwitchDeltaViewCount.objects.create(delta=delta, created_on=datetime.today(),
    #                                influencer_user_profile=influencer_user_profile, campaign_type=campaign_type)

    streams = TwitchDeltaViewCount.objects.filter(created_on__lte=datetime.today(),
                                                  influencer_user_profile=influencer_user_profile, campaign_type=campaign_type).order_by(
        'created_on')

    last_month = datetime.today() - timedelta(days=30)
    last_month_stream = TwitchDeltaViewCount.objects.filter(created_on__gte=last_month,
                                                            influencer_user_profile=influencer_user_profile, campaign_type=campaign_type).order_by(
        'created_on')

    last_two_months = datetime.today() - timedelta(days=60)
    last_two_months_stream = TwitchDeltaViewCount.objects.filter(created_on__gte=last_two_months,
                                                                 created_on__lt=last_month,
                                                                 influencer_user_profile=influencer_user_profile, campaign_type=campaign_type).order_by(
        'created_on')

    last_three_months = datetime.today() - timedelta(days=90)
    last_three_months_stream = TwitchDeltaViewCount.objects.filter(created_on__gte=last_three_months,
                                                                   created_on__lt=last_two_months,
                                                                   influencer_user_profile=influencer_user_profile, campaign_type=campaign_type).order_by(
        'created_on')

    # TODO filter out delta for one particular influencer and compute avg

    number_of_days = 1
    if streams:
        date_diff = datetime.now(timezone.utc) - streams[0].created_on
        number_of_days = date_diff.days
        number_of_days = 1 if number_of_days == 0 else number_of_days

    return last_month_stream, last_two_months_stream, last_three_months_stream, number_of_days


def one_month_avg(last_month_stream):
    last_month_stream_avg = calculate_avg(last_month_stream)
    return last_month_stream_avg


def two_months_avg(last_month_stream, last_two_months_stream):
    last_month_stream_avg = calculate_avg(last_month_stream)
    last_two_months_stream_avg = calculate_avg(last_two_months_stream)
    return last_month_stream_avg, last_two_months_stream_avg


def three_months_avg(last_month_stream, last_two_months_stream, last_three_months_stream):
    last_month_stream_avg = calculate_avg(last_month_stream)
    last_two_months_stream_avg = calculate_avg(last_two_months_stream)
    last_three_months_stream_avg = calculate_avg(last_three_months_stream)
    return last_month_stream_avg, last_two_months_stream_avg, last_three_months_stream_avg

def calculate_avg(streams):

    # first_month_streams = paginator.page(1)
    # first_month_streams_count = first_month_streams.end_index() - first_month_streams.start_index() + 1
    # print('firts month streams are', first_month_streams, first_month_streams.start_index(), first_month_streams.end_index(), first_month_streams_count)

    sum_delta = 0
    for stream in streams:
        sum_delta += stream.delta

    avg = sum_delta/streams.count() if sum_delta else 1

    return avg

def last_month_estimated_payout(last_month_stream_avg, campaign_type):

    if campaign_type == 'overlay':
        amount_estimated = decimal.Decimal(last_month_stream_avg) * decimal.Decimal(OVERLAY_CAMPAIGN_PAYOUT) * 1
    elif campaign_type == 'audio':
        amount_estimated = decimal.Decimal(last_month_stream_avg) * decimal.Decimal(AUDIO_CAMPAIGN_PAYOUT) * 1
    else:
        amount_estimated = decimal.Decimal(last_month_stream_avg) * decimal.Decimal(UNDERLAY_CAMPAIGN_PAYOUT) * 1

    return amount_estimated

def last_two_months_estimated_payout(last_month_stream_avg, last_two_months_stream_avg, campaign_type):

    if campaign_type == 'overlay':
        last_month_stream_amount_estimated = decimal.Decimal(last_month_stream_avg) * decimal.Decimal(OVERLAY_CAMPAIGN_PAYOUT) * decimal.Decimal(0.7)
        last_two_months_stream_amount_estimated = decimal.Decimal(
            last_two_months_stream_avg) * decimal.Decimal(OVERLAY_CAMPAIGN_PAYOUT) * decimal.Decimal(0.3)
        amount_estimated = last_month_stream_amount_estimated + last_two_months_stream_amount_estimated
    elif campaign_type == 'audio':
        last_month_stream_amount_estimated = decimal.Decimal(last_month_stream_avg) * decimal.Decimal(AUDIO_CAMPAIGN_PAYOUT) * decimal.Decimal(0.7)
        last_two_months_stream_amount_estimated = decimal.Decimal(
            last_two_months_stream_avg) * decimal.Decimal(AUDIO_CAMPAIGN_PAYOUT) * decimal.Decimal(0.3)
        amount_estimated = last_month_stream_amount_estimated + last_two_months_stream_amount_estimated
    else:
        last_month_stream_amount_estimated = decimal.Decimal(last_month_stream_avg) * decimal.Decimal(UNDERLAY_CAMPAIGN_PAYOUT) * decimal.Decimal(0.7)
        last_two_months_stream_amount_estimated = decimal.Decimal(
            last_two_months_stream_avg) * decimal.Decimal(UNDERLAY_CAMPAIGN_PAYOUT) * decimal.Decimal(0.3)
        amount_estimated = last_month_stream_amount_estimated + last_two_months_stream_amount_estimated

    return amount_estimated

def last_three_months_estimated_payout(last_month_stream_avg, last_two_months_stream_avg, last_three_months_stream_avg, campaign_type):

    if campaign_type == 'overlay':
        last_month_stream_amount_estimated = decimal.Decimal(last_month_stream_avg) * decimal.Decimal(OVERLAY_CAMPAIGN_PAYOUT) * decimal.Decimal(0.7)
        last_two_months_stream_amount_estimated = decimal.Decimal(
            last_two_months_stream_avg) * decimal.Decimal(OVERLAY_CAMPAIGN_PAYOUT) * decimal.Decimal(0.2)
        last_three_months_stream_amount_estimated = decimal.Decimal(
            last_three_months_stream_avg) * decimal.Decimal(OVERLAY_CAMPAIGN_PAYOUT) * decimal.Decimal(0.1)
        amount_estimated = last_month_stream_amount_estimated + last_two_months_stream_amount_estimated + last_three_months_stream_amount_estimated
    elif campaign_type == 'audio':
        last_month_stream_amount_estimated = decimal.Decimal(last_month_stream_avg) * decimal.Decimal(AUDIO_CAMPAIGN_PAYOUT) * decimal.Decimal(0.7)
        last_two_months_stream_amount_estimated = decimal.Decimal(
            last_two_months_stream_avg) * decimal.Decimal(AUDIO_CAMPAIGN_PAYOUT) * decimal.Decimal(0.2)
        last_three_months_stream_amount_estimated = decimal.Decimal(
            last_three_months_stream_avg) * decimal.Decimal(AUDIO_CAMPAIGN_PAYOUT) * decimal.Decimal(0.1)
        amount_estimated = last_month_stream_amount_estimated + last_two_months_stream_amount_estimated + last_three_months_stream_amount_estimated
    else:
        last_month_stream_amount_estimated = decimal.Decimal(last_month_stream_avg) * decimal.Decimal(UNDERLAY_CAMPAIGN_PAYOUT) * decimal.Decimal(0.7)
        last_two_months_stream_amount_estimated = decimal.Decimal(
            last_two_months_stream_avg) * decimal.Decimal(UNDERLAY_CAMPAIGN_PAYOUT) * decimal.Decimal(0.2)
        last_three_months_stream_amount_estimated = decimal.Decimal(
            last_three_months_stream_avg) * decimal.Decimal(UNDERLAY_CAMPAIGN_PAYOUT) * decimal.Decimal(0.1)
        amount_estimated = last_month_stream_amount_estimated + last_two_months_stream_amount_estimated + last_three_months_stream_amount_estimated

    return amount_estimated

#****************************************************************************************

def get_influencer_twitch_view_count_underlay(influencer, campaign_start_date,
                                              campaign_end_date):
    # return InfluencerTwitchStat.objects.get(
    #     Q(created_on__gte=campaign_start_date) | Q(created_on__lte=campaign_end_date),
    #     influencer_user_profile=influencer)
    influencer_twitch_stats_st = InfluencerTwitchStat.objects.filter(created_on__year=campaign_start_date.year,
                                                                     created_on__month=campaign_start_date.month,
                                                                     created_on__day=campaign_start_date.day,
                                                                     influencer_user_profile=influencer).order_by(
        '-id')

    influencer_twitch_stats_ed = InfluencerTwitchStat.objects.filter(created_on__year=campaign_end_date.year,
                                                                     created_on__month=campaign_end_date.month,
                                                                     created_on__day=campaign_end_date.day,
                                                                     influencer_user_profile=influencer).order_by(
        '-id')

    if influencer_twitch_stats_st.count() > 0:
        influencer_twitch_stats_st = influencer_twitch_stats_st[0]
        st_v_count = influencer_twitch_stats_st.view_count
    else:
        st_v_count = 0

    if influencer_twitch_stats_ed.count() > 0:
        influencer_twitch_stats_ed = influencer_twitch_stats_ed[0]
        ed_v_count = influencer_twitch_stats_ed.view_count
    else:
        ed_v_count = 0

    v_count = abs(ed_v_count - st_v_count)

    # Get the abs diff of view count
    if v_count > 0:
        return v_count
    else:
        return 1  # TODO for now return 1 if the delta view_count is 0


def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i:i + n]


def get_influencer_view_count():
    influencer_list = list(InfluencerUserProfile.objects.all())
    influencer_username_list = [i.get_twitch_username() for i in influencer_list]
    influencer_usernames_list = list(chunks(influencer_username_list, 100))

    view_count = None
    for influencer in influencer_usernames_list:
        print("ding dong")
        profile_data = get_twitch_count(influencer)
        for datum in profile_data:
            id = datum['id']
            login = datum['login']
            display_name = datum['display_name']
            type = datum['type']
            broadcaster_type = datum['broadcaster_type']
            description = datum['description']
            v_count = datum['view_count']
            # if v_count:
            user = User.objects.get(username=datum['display_name'])
            InfluencerTwitchStat.objects.create(twitch_id=id, login=login, display_name=display_name, type=type,
                                                broadcaster_type=broadcaster_type,
                                                description=description, view_count=v_count,
                                                influencer_user_profile=user.profile.influenceruserprofile)

    #cron for saving twitch video api for avg view counts
    for influencer in influencer_usernames_list:
        for username in influencer:
            user_id = get_twitch_user_id(settings.TWITCH_CLIENT_ID, username)
            profile_data = get_twitch_video_view_count(user_id)
            total_count = 0
            total_videos = 0
            avg_count = 1
            print("No ding domh", username, profile_data)
            if profile_data:
                for datum in profile_data:
                    id = datum['id']
                    user_id = datum['user_id']
                    user_name = datum['user_name']
                    title = datum['title']
                    description = datum['description']
                    view_count = datum['view_count']
                    language = datum['language']
                    type = datum['type']
                    duration = datum['duration']
                    print("USERNAME IS", user_name)
                    user = User.objects.get(username=username)
                    print("USER DETIAL", user)

                    total_count += int(datum['view_count'])
                    total_videos += 1
                    if total_count > 0:
                        avg_count = (int(total_count / total_videos))

                    print("AVG COUNT IS THIS", avg_count)

                    InfluencerTwitchVideosStat.objects.create(twitch_id=id, user_id=user_id, user_name=user_name, title=title,
                                                        description=description,
                                                        view_count=view_count, language=language,
                                                        type=type, duration=duration, avg_count=avg_count,
                                                        influencer_user_profile=user.profile.influenceruserprofile)
            else:
                user = User.objects.get(username=username)
                InfluencerTwitchVideosStat.objects.create(avg_count=avg_count,
                                                          influencer_user_profile=user.profile.influenceruserprofile)

def get_twitch_video_view_count(user_id):
    videos_url = "https://api.twitch.tv/helix/videos"
    videos_payload = {'user_id': user_id}
    headers = {'content-type': 'application/x-www-form-urlencoded', 'Authorization': 'Bearer {}'.format(settings.TWITCH_ACCESS_TOKEN), 'Client-ID': settings.TWITCH_CLIENT_ID}
    req = requests.get(videos_url, params=videos_payload, headers=headers).text
    data = json.loads(req)
    twitch_video_view_count = data['data']
    return twitch_video_view_count



def get_twitch_count(username):
    # param_str = f'login={username[0]}'
    # for user in username[1:]:
    #     param_str += f'&login={user}'

    param_str = 'login=' + username[0]
    for user in username[1:]:
        param_str += '&login=' + user
    user_url = "https://api.twitch.tv/helix/users?" + param_str
    payload = {}
    headers = {'content-type': 'application/x-www-form-urlencoded', 'Authorization': 'Bearer {}'.format(settings.TWITCH_ACCESS_TOKEN), 'Client-ID': settings.TWITCH_CLIENT_ID}
    req = requests.get(user_url, params=payload, headers=headers).text
    data = json.loads(req)
    print("data is ", data)
    twitch_view_count = data['data']
    return twitch_view_count


def delete_file(filename):
    try:
        os.remove(filename)
    except OSError:
        pass


def get_video_length(video_file_path):
    result = subprocess.Popen('ffprobe -i ' + video_file_path + ' -show_entries format=duration -v quiet -of csv="p=0"',
                              stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    output = result.communicate()
    return output[0]


def underlay_processing(flask_app_add, underlay_image, provided_underlay_url, submitted_influencer_profile_url, brand,
                        campaign,
                        influencer):
    submitted_influencer_profile_url = submitted_influencer_profile_url.split('/')
    submitted_influencer_profile_url = submitted_influencer_profile_url[1]
    url_ = "/underlay_processing"
    flask_app_add += url_
    data = {'provided_image_url': submitted_influencer_profile_url,
            'provided_underlay_url': provided_underlay_url,
            'submitted_influencer_profile_url': underlay_image,
            'brand': brand,
            'campaign': campaign,
            'influencer': influencer}
    r = requests.post(flask_app_add,
                      data=data)
    LOG.error("r: {0}, {1}".format(r.status_code, r.text))
    print(r.status_code, r.reason, r.text, r.json())
    return r.json()['video_processing_response']


def video_audio_conversion(campaign_type, video_file_path, campaign_id):
    if campaign_type == "overlay":
        root_ = "campaign_videos/"
        ext_ = ".avi"
    # TODO change else to elif campaign_type == "audio" to avoid breakage
    elif campaign_type == "audio":
        root_ = "campaign_audios/"
        ext_ = ".wav"
    else:
        return False

    time_str = time.strftime("_%Y%m%d-%H%M%S")
    head, tail = os.path.split(video_file_path)
    tail = tail.split('.')[0]

    dst_dir = settings.MEDIA_ROOT + root_ + tail + "_" + str(campaign_id) + time_str + ext_
    avi_file = root_ + str(tail) + "_" + str(campaign_id) + time_str + ext_
    avi_exists = os.path.isfile(dst_dir)

    if campaign_type == "overlay":
        cmd_ = "ffmpeg -i %s -crf 20 -c:v libxvid %s" % (video_file_path, dst_dir)
    else:
        cmd_ = "ffmpeg -i %s -acodec pcm_s16le -ac 1 -ar 16000 %s" % (video_file_path, dst_dir)

    if not avi_exists:
        cmds = cmd_.split()
        process = subprocess.Popen(cmds)
        out, err = process.communicate()
        if err:
            return False
        else:
            # Delete the mp4/other than .avi file
            if campaign_type == "overlay":
                delete_file(video_file_path)
            # video_len = get_video_length(dst_dir)
            return avi_file, time_str
    else:
        if campaign_type == "overlay":
            delete_file(video_file_path)
        # video_len = get_video_length(dst_dir)
        return avi_file, time_str


def audio_processing(flask_app_add, uploaded_audio, audio_text, influencer_campaign_submission_id, auth_token):
    print('uploaded audio is %%', uploaded_audio)
    uploaded_audio = uploaded_audio.split('/')
    uploaded_audio = uploaded_audio[1]
    url_ = "/audio_processing"
    flask_app_add += url_
    data = {'uploaded_audio': uploaded_audio,
            'audio_text': audio_text,
            'influencer_campaign_submission_id': influencer_campaign_submission_id,
            'auth_token': auth_token
            }
    # LOG.debug("data")
    # LOG.debug(data)
    # LOG.error("UPLOADED AUDIO: ", uploaded_audio)
    # LOG.debug("UPLOADED AUDIO: ", uploaded_audio)
    # print("UPLOADED AUDIO: ", uploaded_audio)
    try:
        r = requests.post(flask_app_add,
                          data=data)
    except requests.exceptions.ConnectionError:
        return "Max retries exceeded: Connection refused"

    LOG.error("r: {0}, {1}".format(r.status_code, r.text))
    print(r.status_code, r.reason, r.text, r.json())
    return r.json()['video_processing_response']


def video_processing(brand, flask_app_add, uploaded_video,
                     uploaded_image,
                     crop_area, campaign_id, brand_id, influencer_id, influencer_campaign_submission_id, auth_token):
    brand = str(brand).lower()
    # print("brand name {}".format(brand)))
    uploaded_image = uploaded_image.split('/')
    uploaded_image = uploaded_image[1]
    uploaded_video = uploaded_video.split('/')
    uploaded_video = uploaded_video[1]
    url_ = "/video_processing"
    data = {'uploaded_video': uploaded_video,
            'uploaded_image': uploaded_image,
            'crop_area': crop_area,
            'brand': brand,
            'campaign_id': campaign_id,
            'brand_id': brand_id,
            'influencer_id': influencer_id,
            'influencer_campaign_submission_id': influencer_campaign_submission_id,
            'auth_token': auth_token
            }
    flask_app_add += url_
    print('parameter s data  is ****@@@@@@@@', data)
    try:
        r = requests.post(flask_app_add,
                          data=data)
    except requests.exceptions.ConnectionError:
        return "Max retries exceeded: Connection refused"

    LOG.error("r: {0}, {1}".format(r.status_code, r.text))
    print(r.status_code, r.reason, r.text, r.json())
    return r.json()['video_processing_response']


def processed_video_status(campaign, processed_video, influencer_user_profile, campaign_id):
    if processed_video == "True":

        ai_status = "passed"
        payment_status = "pending_payment"
        action = "submission_s2"
        camp_status = "pending_payment"
        if campaign.type == "underlay":
            ai_status = "in_progress"
            payment_status = "not_started"
            action = "submission_ul"
            camp_status = "true"

        # Update the ai_status in the InfluencerCampaignSubmission table
        InfluencerCampaignSubmission.objects.filter(influencer_user_profile=influencer_user_profile,
                                                    campaign_id=campaign_id).update(ai_status=ai_status,
                                                                                    payment_status=payment_status)

        InfluencerCampaign.objects.create(action=action, status=camp_status,
                                          influencer_user_profile=influencer_user_profile,
                                          campaign_id=campaign_id)

    elif processed_video == "Manually True":
        ai_status = "manually_passed"
        payment_status = "manually_pending_payment"
        action = "submission_s2"
        camp_status = "pending_payment"
        if campaign.type == "underlay":
            ai_status = "in_progress"
            payment_status = "not_started"
            action = "submission_ul"
            camp_status = "true"

        # Update the ai_status in the InfluencerCampaignSubmission table
        InfluencerCampaignSubmission.objects.filter(influencer_user_profile=influencer_user_profile,
                                                    campaign_id=campaign_id).update(ai_status=ai_status,
                                                                                    payment_status=payment_status)

        InfluencerCampaign.objects.create(action=action, status=camp_status,
                                          influencer_user_profile=influencer_user_profile,
                                          campaign_id=campaign_id)


    elif processed_video == "False":
        ai_status = "failed"
        payment_status = "campaign_failed"
        action = "submission_s2"
        camp_status = "campaign_failed"
        if campaign.type == "underlay":
            ai_status = "in_progress"
            payment_status = "not_started"
            action = "submission_ul"
            camp_status = "false"
        InfluencerCampaignSubmission.objects.filter(influencer_user_profile=influencer_user_profile,
                                                    campaign_id=campaign_id).update(ai_status=ai_status,
                                                                                    payment_status=payment_status)
        InfluencerCampaign.objects.create(action=action, status=camp_status,
                                          influencer_user_profile=influencer_user_profile,
                                          campaign_id=campaign_id)
        if campaign.type != "underlay":
            # Update escrow and campaign remaining_budget
            update_campaign_budget(campaign, influencer_user_profile, campaign_id)

            # Escrow.objects.filter(influencer_user_profile=influencer_user_profile,
            #                       campaign_id=campaign_id).update(transaction_status="campaign_failed")
    elif processed_video == "Partial True":
        InfluencerCampaignSubmission.objects.filter(influencer_user_profile=influencer_user_profile,
                                                    campaign_id=campaign_id).update(ai_status="partial_pass")
        InfluencerCampaign.objects.create(action='submission_s2', status="manual_intervention",
                                          influencer_user_profile=influencer_user_profile,
                                          campaign_id=campaign_id)
        # Escrow.objects.filter(influencer_user_profile=influencer_user_profile,
        #                       campaign_id=campaign_id).update(transaction_status="campaign_failed")
    else:
        LOG.error('Error: Campaign status not updated.')
        return Response({'error': 'Campaign status not updated.'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

def get_access_token():
    user_url = "https://api.sandbox.paypal.com/v1/oauth2/token"
    payload = {'grant_type': 'client_credentials'}
    headers = {'content-type': 'application/x-www-form-urlencoded'}
    req = requests.post(user_url, params=payload, headers=headers,
                        auth=(settings.PAYPAL_CLIENT_ID,
                             settings.PAYPAL_SECRET_KEY)).text
    data = json.loads(req)
    if data:
        return data['access_token']
    else:
        return None

def batch_payout(influencers_data):
    access_token = get_access_token()
    if access_token:
        payout_url = "https://api.sandbox.paypal.com/v1/payments/payouts"
        headers = {'content-type': 'application/json', 'Authorization': 'Bearer ' + access_token}
        payload = {
            "sender_batch_header": {
                # "sender_batch_id": "Payouts_2018_100007",
                "email_subject": "You have a payout!",
                "email_message": "You have received a payout! Thanks for using our service!"
            },
            "items": influencers_data
        }
        if influencers_data:
            request = requests.post(payout_url, json=payload, headers=headers).text
            return json.loads(request)
        else:
            return None

def payout_batch_details(payout_data):
    access_token = get_access_token()
    if access_token:
        payout_batch_id = payout_data["batch_header"]["payout_batch_id"]
        print("payout_data[batch_header][payout_batch_id]", payout_batch_id)
        payout_batch_details_url = "https://api.sandbox.paypal.com/v1/payments/payouts/" + payout_batch_id
        headers = {'content-type': 'application/json', 'Authorization': 'Bearer ' + access_token}
        request = requests.get(payout_batch_details_url, headers=headers).text
        return json.loads(request)

def save_payout_items(payout_batch_details_data):
    for item in payout_batch_details_data["items"]:
        print("item[transaction_status]", item["transaction_status"], type(item["transaction_status"]))
        print("item[payout_item][amount][value]", item["payout_item"]["amount"]["value"])
        if item["transaction_status"] != "SUCCESS":
            print("NOT SUCCESS")
            payout_batch_id = item["payout_batch_id"]
            payout_item_id = item["payout_item_id"]
            transaction_status = item["transaction_status"]
            withdraw_amount = item["payout_item"]["amount"]["value"]
            email = item["payout_item"]["receiver"]
            PayoutItems.objects.create(payout_batch_id=payout_batch_id, payout_item_id=payout_item_id,
                                      transaction_status=transaction_status, withdraw_amount=withdraw_amount,
                                      email=email)

def payout_item_details(payout_item_id):
    access_token = get_access_token()
    if access_token:
        payout_item_details_url = "https://api.sandbox.paypal.com/v1/payments/payouts-item/" + payout_item_id
        headers = {'content-type': 'application/json', 'Authorization': 'Bearer ' + access_token}
        request = requests.get(payout_item_details_url, headers=headers).text
        return json.loads(request)

from django.db.models import Sum
def paypal_batch_payout():
    today = datetime.now()
    previous_day = today - timedelta(days=1)
    withdraw_amounts = WithdrawAmount.objects.values('user_profile__email', )\
        .annotate(withdraw_amount=Sum('withdraw_amount')).filter(created_on__gte=previous_day, created_on__lte=today)

    influencers_data = []
    count = 0

    payout_items = PayoutItems.objects.all()
    for payout_item in payout_items:
        payout_item_id = payout_item.payout_item_id
        payout_item_data = payout_item_details(payout_item_id)
        if payout_item_data["transaction_status"] == "SUCCESS":
            PayoutItems.objects.filter(payout_item_id=payout_item_id).delete()
        else:
            items = {
                "recipient_type": "EMAIL",
                "amount": {
                    "value": payout_item.withdraw_amount,
                    "currency": "USD"
                },
                "note": "Thanks for your patronage!",
                "receiver": "sb-47d2mk1268413@personal.example.com", #payout_item.email
            }
            influencers_data.append(items)
            PayoutItems.objects.filter(payout_item_id=payout_item_id).delete()
            count += 1
            if count == 999:
                payout_data = batch_payout(influencers_data)
                if payout_data:
                    payout_batch_details_data = payout_batch_details(payout_data)
                    save_payout_items(payout_batch_details_data)
                influencers_data = []
                count = 0


    for withdraw_amount in withdraw_amounts:
        items = {
            "recipient_type": "EMAIL",
            "amount": {
                "value": withdraw_amount["withdraw_amount"],
                "currency": "USD"
            },
            "note": "Thanks for your patronage!",
            # "sender_item_id": "201403140001",
            "receiver": "sb-47d2mk1268413@personal.example.com", #withdraw_amount.user_profile.email or request.user.profile.email
            # "alternate_notification_method": {
            #     "phone": {
            #         "country_code": "91",
            #         "national_number": "9999988888"
            #     }
            # }
        }
        influencers_data.append(items)
        count += 1
        if count == 999:
            payout_data = batch_payout(influencers_data)
            if payout_data:
                payout_batch_details_data = payout_batch_details(payout_data)
                save_payout_items(payout_batch_details_data)
            influencers_data = []
            count = 0

    payout_data = batch_payout(influencers_data)
    if payout_data:
        payout_batch_details_data = payout_batch_details(payout_data)
        # batch_status = payout_batch_details_data["batch_header"]["batch_status"]
        # if batch_status is not "SUCCESS":
        #     payout_batch_id = payout_batch_details_data["batch_header"]["payout_batch_id"]
        #     PayoutItems.object.create(payout_batch_id=payout_batch_id)
        # else:
        save_payout_items(payout_batch_details_data)


def payment_transfer(paypal_account, amount_transfer):
    # Transfer the payment, change the transaction status based on payment made or not
    # TODO return True/False randomly
    return bool(random.getrandbits(1))
    # return True


def payment_validation():
    # Run this after every 1 hour 0 * * * *, for testing every minute * * * *
    # Filter all the submitted campaigns with ai_status = passed and payment_status = pending
    # Check the filtered campaigns time > 48 hours
    # Start the payment procedure
    print("inside payment_validation", str(datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')))
    # forty_eight_hours = datetime.now() - timedelta(days=2)
    one_minute = datetime.now() - timedelta(minutes=1)
    # one_hour = datetime.now() - timedelta(minutes=1)
    submitted_campaigns = InfluencerCampaignSubmission.objects.filter(Q(ai_status="passed") & Q(payment_status="pending_payment") |
                                                                      Q(ai_status="manually_passed") & Q(payment_status="manually_pending_payment"),
                                                                      created_on__lt=one_minute)

    for submitted_campaign in submitted_campaigns:
        print("submitted_campaign: ", submitted_campaign.campaign.name)
        if InfluencerCampaign.objects.filter(Q(action="submission_s2") | Q(action="submission_ul_completed"),
                                             influencer_user_profile=submitted_campaign.influencer_user_profile,
                                             campaign=submitted_campaign.campaign_id,
                                             status="pending_payment").count() == 1:
            if submitted_campaign.ai_status == "manually_passed":
                method_of_payment = 'manual_payment'
                status_ = get_payment_detail(submitted_campaign.influencer_user_profile, submitted_campaign.campaign.id,
                                             method_of_payment)
            else:
                method_of_payment = 'auto_payment'
                status_ = get_payment_detail(submitted_campaign.influencer_user_profile, submitted_campaign.campaign.id,
                                             method_of_payment)
            print("status_: ", status_)
            if not status_:
                submitted_campaign.payment_status = "escrow_missing"
        else:
            submitted_campaign.payment_status = "rejected"
        submitted_campaign.save()

        # if InfluencerCampaign.objects.filter(Q(action="submission_s2") | Q(action="submission_ul_completed") & Q(
        #         influencer_user_profile=submitted_campaign.influencer_user_profile) & Q(campaign=submitted_campaign.campaign_id)
        #          & Q(status="pending_payment")).count() == 1:
        #     status_ = get_payment_detail(submitted_campaign.influencer_user_profile, submitted_campaign.campaign_id)
        #     if not status_:
        #         submitted_campaign.payment_status = "escrow_missing"
        # else:
        #     submitted_campaign.payment_status = "rejected"
        # submitted_campaign.save()

        print("ending payment_validation",
              str(datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')))


def update_campaign_budget(campaign, influencer_user_profile, campaign_id):
    try:
        escrow_obj = Escrow.objects.get(influencer_user_profile=influencer_user_profile,
                                        campaign_id=campaign_id)
    except Escrow.DoesNotExist:
        escrow_obj = None
    if escrow_obj is not None:
        if escrow_obj.amount_estimated:
            campaign.remaining_budget += decimal.Decimal(escrow_obj.amount_estimated)
        escrow_obj.transaction_status = "campaign_failed"
        escrow_obj.save()
        campaign.save()


def get_payment_detail(influencer_user_profile, campaign_id, payment_method):
    # Get the actual_amount/estimated_amount from Escrow
    # Get the paypal account from InfluencerUserProfile
    # Transfer the payment
    # campaign = Campaign.objects.get(id=campaign_id)
    print("inside get_payment_detail", str(datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')))
    try:
        escrow_obj = Escrow.objects.get(influencer_user_profile=influencer_user_profile,
                                        campaign_id=campaign_id)
    except Escrow.DoesNotExist:
        escrow_obj = None

    if escrow_obj is not None:
        if escrow_obj.transaction_status == 'paid':
            return True
        amount_transfer = escrow_obj.amount_actual
        amount_to_be_reversed = 0
        if amount_transfer is None:
            amount_transfer = escrow_obj.amount_estimated
        else:
            amount_to_be_reversed = escrow_obj.amount_estimated - escrow_obj.amount_actual

        print("amount to reversed here it is: ", amount_to_be_reversed)

        # transfer the amount to influencer paypal account
        transfer_status = payment_transfer(influencer_user_profile.paypal_account, amount_transfer)
        print('ending get_payment_detail',
              str(datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')))
        print("transfer stat: ", transfer_status)
        if transfer_status is True:
            escrow_obj.transaction_status = 'paid'
            escrow_obj.amount_actual = amount_transfer
            escrow_obj.save()
            InfluencerCampaign.objects.create(action='cron_payment_transfer', status="paid",
                                              influencer_user_profile=influencer_user_profile,
                                              campaign_id=campaign_id)

            # Update the campaign_submission table
            print("influ payment")
            if payment_method == "auto_payment":
                influencer_payment = InfluencerCampaignSubmission.objects.filter(
                    influencer_user_profile=influencer_user_profile,
                    campaign_id=campaign_id).update(
                    payment_status="paid")
            else:
                influencer_payment = InfluencerCampaignSubmission.objects.filter(
                    influencer_user_profile=influencer_user_profile,
                    campaign_id=campaign_id).update(
                    payment_status="manually_paid")
            print('influencer payment found', influencer_payment)
            campaign = Campaign.objects.get(id=campaign_id)
            print('1) here comes our remaining budget you can see', campaign.remaining_budget, amount_to_be_reversed)
            campaign.remaining_budget += decimal.Decimal(amount_to_be_reversed)
            print('2) here comes our remaining budget you can see', campaign.remaining_budget)
            campaign.save()
            return True

        elif transfer_status is False:
            # Update the ai_status in the InfluencerCampaignSubmission table
            escrow_obj.transaction_status = 'transfer_failed'
            escrow_obj.save()
            InfluencerCampaign.objects.create(action='cron_payment_transfer', status="transfer_failed",
                                              influencer_user_profile=influencer_user_profile,
                                              campaign_id=campaign_id)
            return True
    else:
        return False


