from django.core.management.base import BaseCommand
from influencers.cron import scheduled_twitch_job


class Command(BaseCommand):
    help = 'Run Twitch View Count Cron'

    def handle(self, *args, **options):
        scheduled_twitch_job()
