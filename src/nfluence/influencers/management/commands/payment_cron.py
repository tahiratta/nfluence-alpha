from django.core.management.base import BaseCommand
from influencers.cron import scheduled_job


class Command(BaseCommand):
    help = 'Run Payment Cron'

    def handle(self, *args, **options):
        scheduled_job()
