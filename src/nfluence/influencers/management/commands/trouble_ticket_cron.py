from django.core.management.base import BaseCommand
from influencers.cron import scheduled_trouble_ticket_job


class Command(BaseCommand):
    help = 'Run Trouble Ticket Cron'

    def handle(self, *args, **options):
        scheduled_trouble_ticket_job()
