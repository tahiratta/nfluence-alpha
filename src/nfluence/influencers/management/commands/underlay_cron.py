from django.core.management.base import BaseCommand
from influencers.cron import scheduled_underlay_job


class Command(BaseCommand):
    help = 'Run Underlay Cron'

    def handle(self, *args, **options):
        scheduled_underlay_job()
