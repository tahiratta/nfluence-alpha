from django.core.management.base import BaseCommand
from influencers.cron import scheduled_paypal_job


class Command(BaseCommand):
    help = 'Run Paypal Cron'

    def handle(self, *args, **options):
        scheduled_paypal_job()
