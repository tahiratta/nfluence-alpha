from django.core.management.base import BaseCommand
from influencers.cron import scheduled_under_review_campaigns

class Command(BaseCommand):
    help = 'Run Under Review Campaigns Cron'

    # def __init__(self, *args, **kwargs):
    #     super(Command, self).__init__(*args, **kwargs)
    #     scheduled_under_review_campaigns(self)
    def handle(self, *args, **options):
        scheduled_under_review_campaigns()
