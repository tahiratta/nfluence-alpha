#import os
#from nfluence import settings
#from nfluence.celery import app
#from celery import current_task
#from influencers.utils import (
#    video_audio_conversion,
#    video_processing,
#    audio_processing,
#    processed_video_status,
#    underlay_processing,
#    update_escrow
#)
#from django.contrib.sites.models import Site
#from campaigns.models import (
#    Campaign
#)
#from influencers.models import (
#    InfluencerCampaign,
#    InfluencerUserProfile,
#    InfluencerCampaignSubmission,
#    InfluencerTwitchStat
#)
#from campaigns.utils import (
#    get_twitch_video_id,
#    get_twitch_video_details
#)
#from payments.models import (
#    Escrow
#)
#from datetime import datetime, timedelta
#import logging
#
#LOG = logging.getLogger('nfluence.influencers.tasks')
#
#DOMAIN = os.environ.get('NFLUENCE_HOST_DOMAIN', 'http://localhost:8000')
#
## DOMAIN = os.environ.get('NFLUENCE_HOST_DOMAIN', 'http://nginx')
#
#
## @app.task
## def celery_test_func(n):
##     for i in range(int(n)):
##         current_task.update_state(state='PROGRESS',
##                                   meta={'output': 'test_output {0}'.format(i)})
##
##     return {'output': 'Completed the test task'}
#
#
#@app.task
#def celery_backend(campaign_id, influencer_campaign_submission_id, influencer_user_profile_id, auth_token):
#    response = {}
#    processed_video = None
#    CAMPAIGN_ASSET_URLS = dict()
#    campaign = Campaign.objects.get(id=campaign_id)
#    influencer_campaign_submission = InfluencerCampaignSubmission.objects.get(id=influencer_campaign_submission_id)
#    influencer_user_profile = InfluencerUserProfile.objects.get(id=influencer_user_profile_id)
#
#    if campaign.type == "overlay" or campaign.type == "audio":
#        # Convert the video to .avi
#        if influencer_campaign_submission.uploaded_video.path:
#            avi_file, time_str = video_audio_conversion(campaign.type,
#                                                        influencer_campaign_submission.uploaded_video.path,
#                                                        campaign.id)
#
#    if campaign.type == "overlay":
#        # If converted successfully, update the extension of the video
#        if avi_file:
#            influencer_campaign_submission.uploaded_video = avi_file
#            influencer_campaign_submission.save()
#
#        #CAMPAIGN_ASSET_URLS['SUBMITTED_VIDEO_URL'] = DOMAIN + influencer_campaign_submission.uploaded_video.url
#        CAMPAIGN_ASSET_URLS['SUBMITTED_VIDEO_URL'] = str(influencer_campaign_submission.uploaded_video.name)
#        #CAMPAIGN_ASSET_URLS[
#        #    'SUBMITTED_CAMPAIGN_LOGO_URL'] = DOMAIN + influencer_campaign_submission.campaign.campaign_logo.url
#        CAMPAIGN_ASSET_URLS[
#            'SUBMITTED_CAMPAIGN_LOGO_URL'] = str(influencer_campaign_submission.campaign.campaign_logo)
#
#        # Calling the flask app for video processing
#        processed_video = video_processing(campaign.brand.name,
#                                           settings.FLASK_APP_ML_OVERLAY,
#                                           CAMPAIGN_ASSET_URLS['SUBMITTED_VIDEO_URL'],
#                                           CAMPAIGN_ASSET_URLS['SUBMITTED_CAMPAIGN_LOGO_URL'],
#                                           influencer_campaign_submission.campaign.logo_position,
#                                           campaign.id,
#                                           campaign.brand.id,
#                                           influencer_campaign_submission.influencer_user_profile.id,
#                                           influencer_campaign_submission_id,
#                                           auth_token)
#    elif campaign.type == "audio":
#        if avi_file:
#            influencer_campaign_submission.uploaded_audio = avi_file
#            influencer_campaign_submission.save()
#        #CAMPAIGN_ASSET_URLS['SUBMITTED_AUDIO_URL'] = DOMAIN + influencer_campaign_submission.uploaded_audio.url
#        CAMPAIGN_ASSET_URLS['SUBMITTED_AUDIO_URL'] = str(influencer_campaign_submission.uploaded_audio.name)
#        campaign_audio_file_text = campaign.campaign_audio_text_file
#        if campaign.campaign_script:
#            audio_campaign_text = campaign.campaign_script
#        elif campaign_audio_file_text:
#            audio_campaign_text = campaign_audio_file_text.readlines()
#            x = [x.decode('utf-8') for x in audio_campaign_text]
#            x_ = [elem.strip("\n") for elem in x]
#            audio_campaign_text = ' '.join(x_)
#        else:
#            audio_campaign_text = "Audio not found!"
#
#        # Calling the flask app for video processing
#        processed_video = audio_processing(settings.FLASK_APP_ML_AUDIO,
#                                           CAMPAIGN_ASSET_URLS['SUBMITTED_AUDIO_URL'],
#                                           audio_campaign_text, influencer_campaign_submission_id, auth_token)
#
#    else:
#        CAMPAIGN_ASSET_URLS['SUBMITTED_UNDERLAY_URL'] = influencer_campaign_submission.video_url
#        #CAMPAIGN_ASSET_URLS[
#        #    'SUBMITTED_UNDERLAY_IMAGE_URL'] = DOMAIN + influencer_campaign_submission.campaign.campaign_logo.url
#        CAMPAIGN_ASSET_URLS[
#            'SUBMITTED_UNDERLAY_IMAGE_URL'] = str(influencer_campaign_submission.campaign.campaign_logo)
#        processed_video = underlay_processing(
#            settings.FLASK_APP_ML_UNDERLAY,
#            CAMPAIGN_ASSET_URLS['SUBMITTED_UNDERLAY_URL'],
#            campaign.campaign_underlay_url,
#            CAMPAIGN_ASSET_URLS['SUBMITTED_UNDERLAY_IMAGE_URL'],
#            campaign.brand.id,
#            campaign.id,
#            influencer_user_profile_id
#        )
#
#    influencer_campaign_submission.save()
#
#    # Process the payment if machine learning test is passes
#    # Change the payment status
#    if processed_video in ["True", "False", "Partial True"]:
#        update_escrow(processed_video, campaign, influencer_campaign_submission, influencer_user_profile)
#        processed_video_status(campaign, processed_video, influencer_user_profile, campaign.id)
#    influencer_campaign = InfluencerCampaign.objects.filter(campaign=campaign,
#                                                            influencer_user_profile=influencer_user_profile) \
#        .order_by('-id')[0]
#    response['status'] = influencer_campaign.status
#    response['submission_id'] = influencer_campaign_submission.id
#    return response
