from django.db import models
from django.contrib.postgres.fields import citext
from campaigns.models import (
    Campaign
)
from accounts.models import (
    InfluencerUserProfile,
    UserProfile
)
from nfluence.models import NfluenceBaseModel
from django.conf import settings
from datetime import datetime


class InfluencerCampaign(NfluenceBaseModel):
    action = citext.CICharField(max_length=25, blank=True, null=True)
    status = citext.CICharField(max_length=25, blank=True, null=True)
    # twitch_image_link_to = citext.CICharField(max_length=255, blank=True, null=True)
    influencer_user_profile = models.ForeignKey(InfluencerUserProfile, on_delete=models.CASCADE)
    campaign = models.ForeignKey(Campaign, on_delete=models.CASCADE)


class InfluencerCampaignSubmission(NfluenceBaseModel):
    AI_STATUS_CHOICES = (
        ('pending', 'Pending'),
        ('passed', 'Passed'),
        ('manually_passed', 'Manually Passed'),
        ('failed', 'Failed'),
        ('partial_pass', 'Partial Pass'),
    )
    PAYMENT_STATUS_CHOICES = (
        ('not_started', 'Not Started'),
        ('pending_payment', 'Pending Payment'),
        ('manually_pending_payment', 'Manually Pending Payment'),
        ('campaign_failed', 'Campaign Failed'),
        ('paid', 'Paid'),
        ('manually_paid', 'Manually Paid'),
    )
    CAMPAIGN_TYPE_CHOICES = (
        ('overlay', 'Overlay'),
        ('underlay', 'Underlay'),
        ('audio', 'Audio'),
    )

    video_url = citext.CICharField(max_length=225, blank=True, null=True)
    comments = citext.CITextField(blank=True, null=True)
    celery_task_id = citext.CICharField(max_length=225, blank=True, null=True)
    ai_status = citext.CICharField(max_length=225, choices=AI_STATUS_CHOICES, default='pending')
    payment_status = citext.CICharField(max_length=225, choices=PAYMENT_STATUS_CHOICES, default='not_started')
    uploaded_video = models.FileField(upload_to='campaign_videos/', null=True, verbose_name="")
    uploaded_audio = models.FileField(upload_to='campaign_audios/', null=True, verbose_name="")
    influencer_user_profile = models.ForeignKey(InfluencerUserProfile, on_delete=models.CASCADE)
    campaign = models.ForeignKey(Campaign, on_delete=models.CASCADE)

    def submitted_media(self):
        if self.uploaded_video:
            return settings.MEDIA_URL + str(self.uploaded_video)
        if self.uploaded_audio:
            return settings.MEDIA_URL + str(self.uploaded_audio)
        return None

    # class Meta:
    #     unique_together = ('influencer_user_profile', 'campaign')


class InfluencerCampaignSubmissionAudioConversions(NfluenceBaseModel):
    campaign_script = models.TextField(null=True, blank=True)
    speech_to_text = models.TextField(null=True, blank=True)
    joint_words_script = models.TextField(null=True, blank=True)
    short_script = models.TextField(null=True, blank=True)
    soundex_text = models.TextField(null=True, blank=True)
    short_script_result = models.CharField(max_length=255, null=True, blank=True)
    soundex_text_result = models.CharField(max_length=255, null=True, blank=True)
    submission = models.ForeignKey(InfluencerCampaignSubmission, on_delete=models.CASCADE)


class InfluencerCampaignSubmissionOverlay(NfluenceBaseModel):
    total_no_of_detected_logo_frames = models.TextField(null=True, blank=True)
    no_of_dl_detected_logo_frames = models.TextField(null=True, blank=True)
    total_probability_of_detected_logo = models.TextField(null=True, blank=True)
    no_of_ORB_detected_logo_frames = models.TextField(null=True, blank=True)
    campaign_result = models.TextField(null=True, blank=True)
    submission = models.ForeignKey(InfluencerCampaignSubmission, on_delete=models.CASCADE)


class InfluencerCampaignSubmissionOverlayFrames(NfluenceBaseModel):
    uploaded_frame = models.TextField(null=True, blank=True)
    orb_uploaded_frame = models.TextField(null=True, blank=True)
    dl_uploaded_frame = models.TextField(null=True, blank=True)
    # submission = models.ForeignKey(InfluencerCampaignSubmission, on_delete=models.CASCADE)
    influencer_campaign_submission = models.ForeignKey(InfluencerCampaignSubmissionOverlay, on_delete=models.CASCADE)


class InfluencerTwitchStat(NfluenceBaseModel):
    twitch_id = models.IntegerField(blank=True, null=True)
    login = models.CharField(max_length=225, blank=True, null=True)
    display_name = models.CharField(max_length=225, blank=True, null=True)
    type = models.CharField(max_length=225, blank=True, null=True)
    broadcaster_type = models.CharField(max_length=225, blank=True, null=True)
    description = models.CharField(max_length=225, blank=True, null=True)
    view_count = models.IntegerField()
    influencer_user_profile = models.ForeignKey(InfluencerUserProfile, on_delete=models.CASCADE)

#saving twitch video api data for avg view counts
class InfluencerTwitchVideosStat(NfluenceBaseModel):
    twitch_id = models.IntegerField(blank=True, null=True)
    user_id = models.IntegerField(blank=True, null=True)
    user_name = models.CharField(max_length=225, blank=True, null=True)
    title = models.CharField(max_length=225, blank=True, null=True)
    language = models.CharField(max_length=225, blank=True, null=True)
    description = models.CharField(max_length=225, blank=True, null=True)
    type = models.CharField(max_length=225, blank=True, null=True)
    duration = models.CharField(max_length=225, blank=True, null=True)
    view_count = models.IntegerField(null=True, blank=True)
    avg_count = models.IntegerField()
    influencer_user_profile = models.ForeignKey(InfluencerUserProfile, on_delete=models.CASCADE)


class TwitchDeltaViewCount(NfluenceBaseModel):
    delta = models.IntegerField()
    influencer_user_profile = models.ForeignKey(InfluencerUserProfile, on_delete=models.CASCADE)
    campaign_type = models.CharField(max_length=225, blank=True, null=True)

class SubmitTicket(NfluenceBaseModel):
    ticket_category = models.CharField(max_length=255, blank=True, null=True)
    ticket_status = models.CharField(max_length=255, default="To Do")
    ticket_submitted_status = models.CharField(max_length=255, default="submitted")
    description_of_problem = models.TextField(max_length=5000, blank=True, null=True)
    # attachments = models.FileField(blank=True, null=True)
    influencer_email = models.EmailField(max_length=255, blank=True, null=True)
    influencer_name = models.CharField(max_length=255, blank=True, null=True)
    user_profile = models.ForeignKey(UserProfile, on_delete=models.CASCADE)
    campaign = models.ForeignKey(Campaign, on_delete=models.CASCADE, null=True, blank=True)
    submission_date = models.DateField(null=True, blank=True)
    observation_date = models.DateField(null=True, blank=True)
    observation_time = models.TimeField(null=True, blank=True)

class SubmitTicketImage(NfluenceBaseModel):
    submit_ticket = models.ForeignKey(SubmitTicket, on_delete=models.CASCADE)
    attachment = models.FileField(blank=True)
