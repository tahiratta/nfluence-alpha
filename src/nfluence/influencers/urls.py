from rest_framework import routers
from django.urls import path, include
from influencers.views import (
    SubmittedCampaignsViewSet,
    InfluencerCampaignViewSet,
    InfluencerCampaignView,
    InfluencerCampaignSubmissionView,
    # InfluencerCampaignStatus,
    #InfluencerCampaignCeleryView,
    InfluencerCampaignSubmissionAudioConversionsViewset,
    InfluencerCampaignSubmissionOverlayViewset,
    InfluencerCampaignSubmissionOverlayFramesViewset,
    InfluencerCampaignHistoryViewSet,
    SubmitTicketViewSet,
    CompanySubmitTicketViewSet,
    InfluencerCampaignSubmissionUnderlayTrailView,
    EarningsWithdrawalViewset,
    HighestEscrowBrandsView,
    TopBrandPercentagesView
)

router = routers.DefaultRouter()
router.register(r'submitted_campaigns', SubmittedCampaignsViewSet, basename='submitted_campaigns')
router.register(r'influencer_campaigns', InfluencerCampaignViewSet, basename='influencer_campaigns')
router.register(r'influencer_campaigns_history', InfluencerCampaignHistoryViewSet, basename='influencer_campaigns')
router.register(r'influencer_campaign_submission_audio_conversions',
                InfluencerCampaignSubmissionAudioConversionsViewset,
                basename='influencer_campaign_submission_audio_conversions')
router.register(r'influencer_campaign_submission_audit_overlay',
                InfluencerCampaignSubmissionOverlayViewset,
                basename='influencer_campaign_submission_audit_overlay')
router.register(r'influencer_campaign_submission_audit_overlay_frames',
                InfluencerCampaignSubmissionOverlayFramesViewset,
                basename='influencer_campaign_submission_audit_overlay_frames')

router.register(r'earnings_withdrawal', EarningsWithdrawalViewset, basename='earnings_withdrawal')

# router.register(r'influencer_campaigns_celery', InfluencerCampaignCeleryView, basename='influencer_campaigns_celery')
router.register(r'submit_ticket', SubmitTicketViewSet, basename='submit_Ticket')
router.register(r'company_submit_ticket', CompanySubmitTicketViewSet, basename='company_submit_Ticket')

urlpatterns = [
    path('', include(router.urls)),
    path('influencer/campaigns', InfluencerCampaignView.as_view()),
    path('influencer/campaign/submit', InfluencerCampaignSubmissionView.as_view()),
    #path('influencer/campaigns/celery', InfluencerCampaignCeleryView.as_view()),
    path('influencer_campaign_submission_audit_underlay_trail', InfluencerCampaignSubmissionUnderlayTrailView.as_view()),
    path('influencer/highest_escrow_brands', HighestEscrowBrandsView.as_view()),
    path('influencer/top_brands_percentages', TopBrandPercentagesView.as_view()),
    # path('sentry-debug/', trigger_error),
    # path('influencer/campaign/submission/status', InfluencerCampaignStatus.as_view()),
]
