# Generated by Django 2.2.5 on 2020-02-25 15:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('influencers', '0006_auto_20200225_1630'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='submitticket',
            name='influencer_user_profile',
        ),
        migrations.AddField(
            model_name='submitticket',
            name='influencer_email',
            field=models.EmailField(blank=True, max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='submitticket',
            name='influencer_name',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name='submitticket',
            name='ticket_category',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
    ]
