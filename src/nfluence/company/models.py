from django.db import models
from django.contrib.postgres.fields import citext
from nfluence.models import NfluenceBaseModel


class Company(NfluenceBaseModel):
    """Company model

      Creates:
          A company table with the specified fields
      name:
         The name of the company
      address_line_1:
         The first address of the company
      address_line_2:
         The second address of the company
      city:
         The city of the company
      zip_code:
         The zip code of the company
      """
    name = citext.CICharField(max_length=255)
    address_line_1 = citext.CICharField(max_length=255)
    address_line_2 = citext.CICharField(max_length=255, blank=True, null=True)
    city = citext.CICharField(max_length=255)
    zip_code = citext.CICharField(max_length=255)

    class Meta:
        verbose_name_plural = "Companies"
