from rest_framework import viewsets

from company.models import Company
from company.serializers import CompanySerializer


class CompanyViewSet(viewsets.ModelViewSet):
    """
       A simple ViewSet for retrieving companies.
      """
    queryset = Company.objects.all()
    serializer_class = CompanySerializer
