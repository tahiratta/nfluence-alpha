from django.contrib import admin
from company.models import Company


@admin.register(Company)
class CompanyAdmin(admin.ModelAdmin):
    """CompanyAdmin serializer
      Purpose:
          To register the model class
      """
    list_display = ('name', 'address_line_1', 'address_line_2', 'city', 'zip_code')
    # add filtering by yearly_budget
    list_filter = ('city',)
    # add search fields
    search_fields = ['name', 'address_line_1', 'city', 'zip_code']

