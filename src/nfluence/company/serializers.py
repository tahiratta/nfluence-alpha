from rest_framework import serializers
from company.models import (
    Company
)


class CompanySerializer(serializers.ModelSerializer):
    """Company serializer

      Creates:
          A company serializer to translate the company model
      model:
         The name of the company model
      fields:
         The fields required to be translated from the company model
      """
    class Meta:
        model = Company
        fields = ('name', 'address_line_1', 'address_line_2', 'city', 'zip_code')
