from rest_framework import routers
from django.urls import path, include
from company.views import (
    CompanyViewSet
)


router = routers.DefaultRouter()
router.register(r'company', CompanyViewSet)

urlpatterns = [
    path('', include(router.urls))
]
