from rest_framework import viewsets
from rest_framework.views import APIView

from campaigns.models import Campaign
from campaigns.serializers import CampaignSerializer
from influencers.models import InfluencerCampaign
from influencers.serializers import InfluencerSerializer
from rest_framework import status as rest_status
from django.http.response import JsonResponse
from django.contrib.auth import get_user_model
from payments.models import Escrow
import decimal

User = get_user_model()


class CampaignViewSet(viewsets.ModelViewSet):
    queryset = Campaign.objects.all()
    serializer_class = CampaignSerializer

    # TODO add remaining_budget = yearly_budget for the campaign

    # def get_queryset(self):
    #     """
    #     filtering against a `brand_id` query parameter in the URL.
    #     """
    #     queryset = Campaign.objects.all()
    #     brand_id = self.request.query_params.get('brand', None)
    #     if brand_id is not None:
    #         queryset = queryset.filter(brand_id=brand_id)
    #     return queryset


class CampaignsHistoryView(APIView):

    @staticmethod
    def get(requets, *args, **kwargs):
        campaigns = Campaign.objects.all()
        # data = {}
        completed_campaigns = []
        incompleted_campaigns = []
        # completed_campaigns = {}
        # other_campaigns = {}
        for campaign in campaigns:
            num_of_days = ''
            if campaign.type == "underlay":
                if campaign.num_of_days:
                    num_of_days = campaign.num_of_days
            campaign_budget = campaign.campaign_budget
            remaining_budget = campaign.remaining_budget

            # remaining_budget is null so converting it into decimals
            if remaining_budget is None:
                remaining_budget = decimal.Decimal(0.00)

            # Logic for getting no. of influencers
            no_of_influencers = CampaignSerializer.get_influencer_count(campaign)

            # check if remaining budget is exhausted or not
            is_exhausted = (((campaign_budget - remaining_budget) / campaign_budget * 100) >= decimal.Decimal(90.00))

            if is_exhausted or campaign.status == "completed":
                # Paid out amount logic, only return when budget is exhausted
                escrow = Escrow.objects.filter(campaign=campaign, transaction_status='paid')
                paid_out_amount = 0
                for esc in escrow:
                    amount = esc.amount_estimated
                    if esc.amount_actual:
                        amount = esc.amount_actual
                    paid_out_amount += amount
                paid_out = paid_out_amount

                # change status to complete if remaining budget is exhausted
                campaign.status = "completed"
                campaign.save()
                completed_campaigns_dict = {
                    'id': campaign.id,
                    'name': campaign.name,
                    'brand': campaign.brand.id,
                    'type': campaign.type,
                    'status': campaign.status,
                    'no_of_influencers': no_of_influencers,
                    'paid_out_amount': paid_out,
                    'num_of_days': num_of_days,
                    'last_updated': campaign.updated_on,

                    "campaign_budget": campaign.campaign_budget,
                    "campaign_logo": str(campaign.campaign_logo),
                    "logo_position": campaign.logo_position,
                    "campaign_script": campaign.campaign_script,
                    "type": campaign.type,
                    "expiry": campaign.expiry,
                    "start_date": campaign.start_date,
                    "end_date": campaign.end_date,
                    "remaining_budget": campaign.remaining_budget,
                    "followers_count": campaign.followers_count,
                    "description": campaign.description,
                    "created_on": campaign.created_on,
                    "updated_on": campaign.updated_on,
                    "campaign_brand_name": campaign.brand.name,
                    "campaign_audio_text_file": str(campaign.campaign_audio_text_file),
                    "campaign_underlay_url": campaign.campaign_underlay_url,
                    "influencer_count": no_of_influencers

                }
                completed_campaigns.append(completed_campaigns_dict)
            else:
                other_campaigns_dict = {
                    'id': campaign.id,
                    'name': campaign.name,
                    'brand': campaign.brand.id,
                    'type': campaign.type,
                    'status': campaign.status,
                    'no_of_influencers': no_of_influencers,
                    'num_of_days': num_of_days,
                    'last_updated': campaign.updated_on,

                    "campaign_budget": campaign.campaign_budget,
                    "campaign_logo": str(campaign.campaign_logo),
                    "logo_position": campaign.logo_position,
                    "campaign_script": campaign.campaign_script,
                    "type": campaign.type,
                    "expiry": campaign.expiry,
                    "start_date": campaign.start_date,
                    "end_date": campaign.end_date,
                    "remaining_budget": campaign.remaining_budget,
                    "followers_count": campaign.followers_count,
                    "description": campaign.description,
                    "created_on": campaign.created_on,
                    "updated_on": campaign.updated_on,
                    "campaign_brand_name": campaign.brand.name,
                    "campaign_audio_text_file": str(campaign.campaign_audio_text_file),
                    "campaign_underlay_url": campaign.campaign_underlay_url,
                    "influencer_count": no_of_influencers
                }

                incompleted_campaigns.append(other_campaigns_dict)
        data = {
            'completed_campaigns': completed_campaigns,
            'other_campaigns': incompleted_campaigns
        }
        return JsonResponse(data)


class CampaignInfluencerView(APIView):

    def get(self, request, *args, **kwargs):
        status = None
        response = {}

        campaign_id = None
        try:
            campaign_id = int(kwargs.get('id', None))
        except ValueError as ve:
            status = False
            response['message'] = 'campaign must be a valid integer'

        campaign = None
        try:
            campaign = Campaign.objects.get(id=campaign_id)
        except Campaign.DoesNotExist:
            status = False
            response['message'] = 'Invalid Campaign ID'

        if campaign_id and campaign:
            response['influencers'] = list()

            passed_influencers = []

            campaign_influencers = InfluencerCampaign.objects.filter(campaign=campaign).order_by('-id')

            for ci in campaign_influencers:
                influencer = {}
                if ci.influencer_user_profile.id not in passed_influencers:
                    user = User.objects.get(profile=ci.influencer_user_profile.user_profile)
                    escrow = Escrow.objects.filter(influencer_user_profile=ci.influencer_user_profile,
                                                   campaign=campaign).order_by('-id')

                    influencer['id'] = user.id
                    influencer['name'] = user.get_full_name()
                    influencer['campaign_type'] = campaign.type

                    if escrow.count() > 0:
                        influencer['estimated_payout'] = escrow[0].amount_estimated
                        # influencer['actual_payout'] = escrow[0].amount_estimated
                        if escrow[0].amount_actual:
                            influencer['actual_payout'] = escrow[0].amount_actual
                    else:
                        influencer['estimated_payout'] = 0
                        influencer['actual_payout'] = 0
                    influencer['user_status'] = user.get_user_status()

                    passed_influencers.append(ci.influencer_user_profile.id)
                    response['influencers'].append(influencer)

            status = True

            # data = [
            #     InfluencerSerializer(model).data
            #     for model in campaign_influencers
            # ]
            # status = True
            # response['influencers'] = data

        return JsonResponse({'status': status, 'message': response})
