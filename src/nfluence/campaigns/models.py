from django.db import models
from django.contrib.postgres.fields import citext
from brands.models import Brand
from nfluence.models import NfluenceBaseModel


class Campaign(NfluenceBaseModel):
    STATUS_CHOICES = (
        ('pending_approval', 'Pending Approval'),
        ('active', 'Active'),
        ('completed', 'Completed'),
        ('paused', 'Paused'),
        ('scheduled', 'Scheduled'),
        ('expired', 'Expired'),
        ('cancelled', 'Cancelled'),
    )

    name = citext.CICharField(max_length=255)
    campaign_budget = models.DecimalField(blank=True, null=True, max_digits=10, decimal_places=2)
    campaign_logo = models.ImageField(upload_to='campaign_logos/', blank=True, null=True)
    campaign_audio_text_file = models.FileField(upload_to='campaign_audio_texts/', blank=True, null=True)
    campaign_underlay_url = citext.CITextField(blank=True, null=True)
    campaign_script = citext.CITextField(blank=True, null=True)
    num_of_days = models.IntegerField(blank=True, null=True)
    logo_position = models.IntegerField(blank=True, null=True)
    type = citext.CICharField(max_length=255, blank=True, null=True)
    status = citext.CICharField(max_length=255, choices=STATUS_CHOICES, default="pending_approval")
    status_last_updated = models.DateTimeField(verbose_name="Campaign Status Updated Date", blank=True, null=True)
    expiry = models.DateTimeField(verbose_name="Campaign Expiry Datetime", blank=True, null=True)
    start_date = models.DateField(verbose_name="Scheduled Start Date", blank=True, null=True)
    end_date = models.DateField(verbose_name="Scheduled End Date", blank=True, null=True)
    remaining_budget = models.DecimalField(blank=True, null=True, max_digits=10, decimal_places=2)
    followers_count = models.IntegerField(default=0)
    description = citext.CICharField(max_length=255, blank=True, null=True)
    brand = models.ForeignKey(Brand, on_delete=models.CASCADE)

    def clean(self):
        self.name = self.name.capitalize()

    class Meta:
        unique_together = ("name", "brand")
