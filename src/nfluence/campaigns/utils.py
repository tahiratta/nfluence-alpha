import requests
import json
from django.conf import settings


def get_twitch_video_details(video_id):
    twitch_clip_url = "https://api.twitch.tv/helix/videos?id=" + video_id
    headers = {'content-type': 'application/x-www-form-urlencoded', 'Authorization': 'Bearer {}'.format(settings.TWITCH_ACCESS_TOKEN), 'Client-ID': settings.TWITCH_CLIENT_ID}
    req = requests.get(twitch_clip_url, headers=headers).text
    data = json.loads(req)
    if data['data']:
        created_at = data['data'][0]['created_at']
        duration = data['data'][0]['duration']
        if created_at and duration:
            return created_at, duration
        else:
            return None, None
    else:
        return None


def get_twitch_video_id(clip_url):

    clip_id = clip_url.split('/')[-1]
    twitch_clip_url = "https://api.twitch.tv/helix/clips?id=" + clip_id
    headers = {'content-type': 'application/x-www-form-urlencoded', 'Authorization': 'Bearer {}'.format(settings.TWITCH_ACCESS_TOKEN), 'Client-ID': settings.TWITCH_CLIENT_ID}
    req = requests.get(twitch_clip_url, headers=headers).text
    data = json.loads(req)
    if data['data']:
        video_id = data['data'][0]['video_id']
        if video_id:
            return video_id
        else:
            return None
    else:
        return None


def get_twitch_user_id(client_id, username):
    user_url = "https://api.twitch.tv/helix/users"
    payload = {'login': username}
    headers = {'content-type': 'application/x-www-form-urlencoded', 'Authorization': 'Bearer {}'.format(settings.TWITCH_ACCESS_TOKEN), 'Client-ID': client_id}
    req = requests.get(user_url, params=payload, headers=headers).text
    data = json.loads(req)
    if data['data']:
        twitch_user_id = data['data'][0]['id']
        if twitch_user_id:
            return twitch_user_id
    else:
        return None


def get_twitch_followers(client_id, user_id):
    followers_url = "https://api.twitch.tv/helix/users/follows"
    followers_payload = {'to_id': user_id}
    headers = {'content-type': 'application/x-www-form-urlencoded', 'Authorization': 'Bearer {}'.format(settings.TWITCH_ACCESS_TOKEN), 'Client-ID': client_id}
    req = requests.get(followers_url, params=followers_payload, headers=headers).text
    data = json.loads(req)
    total_followers = data['total']
    return total_followers


def get_average_view_count(client_id, user_id):
    total_count = 0
    total_videos = 0
    avg_count = 1
    videos_url = "https://api.twitch.tv/helix/videos"
    videos_payload = {'user_id': user_id}
    headers = {'content-type': 'application/x-www-form-urlencoded', 'Authorization': 'Bearer {}'.format(settings.TWITCH_ACCESS_TOKEN), 'Client-ID': client_id}
    req = requests.get(videos_url, params=videos_payload, headers=headers).text
    data = json.loads(req)
    if not 'error' in data:
        for elem in data['data']:
            total_count += int(elem['view_count'])
            total_videos += 1
        if total_count > 0:
            avg_count = (int(total_count / total_videos))
    return avg_count
