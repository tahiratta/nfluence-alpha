from rest_framework import serializers
from campaigns.models import (
    Campaign
)
from payments.models import (
    Escrow
)
from influencers.models import InfluencerCampaign#, SubmitTicket
import datetime

# from accounts.models import (
#     # UserProfile,
#     InfluencerUserProfile
# )


class CampaignSerializer(serializers.ModelSerializer):
    campaign_brand_name = serializers.SerializerMethodField()
    influencer_count = serializers.SerializerMethodField()
    escrow_amount = serializers.SerializerMethodField()
    paid_out_amount = serializers.SerializerMethodField()
    status_last_updated = serializers.SerializerMethodField()
    influencer_status = serializers.CharField(required=False, read_only=True)
    # influencer_status = serializers.SerializerMethodField(required=False)
    submitted_media = serializers.CharField(required=False, read_only=True)

    # @staticmethod
    # def get_influencer_status(obj):
    #     print('object is', obj)
    #     submit_ticket = SubmitTicket.objects.filter(campaign=obj)
    #     influencer_status = None
    #     if submit_ticket:
    #         influencer_user_profile = InfluencerUserProfile.objects.get(user_profile=submit_ticket[0].user_profile)
    #         ic = InfluencerCampaign.objects.filter(influencer_user_profile=influencer_user_profile,
    #                                                campaign=obj).order_by('-id')
    #         influencer_status = ic[0].status
    #
    #     return influencer_status

    @staticmethod
    def get_campaign_brand_name(obj):
        return obj.brand.name

    @staticmethod
    def get_influencer_count(obj):
        unique_influencers = []
        ics = InfluencerCampaign.objects.filter(campaign=obj)
        for ic in ics:
            if ic.influencer_user_profile.id not in unique_influencers:
                unique_influencers.append(ic.influencer_user_profile.id)

        return len(unique_influencers)

    @staticmethod
    def get_escrow_amount(obj):
        esc = Escrow.objects.filter(campaign=obj, transaction_status='pending_processing')
        amount = 0
        for e in esc:
            a = e.amount_estimated
            if e.amount_actual:
                a = e.amount_actual
            amount += a
        return amount

    @staticmethod
    def get_paid_out_amount(obj):
        esc = Escrow.objects.filter(campaign=obj, transaction_status='paid')
        amount = 0
        for e in esc:
            a = e.amount_estimated
            if e.amount_actual:
                a = e.amount_actual
            amount += a
        return amount

    @staticmethod
    def get_status_last_updated(obj):
        return datetime.datetime.today()

    def create(self, validated_data):
        obj = Campaign.objects.create(**validated_data)
        obj.remaining_budget = validated_data['campaign_budget']
        obj.save()
        return obj
        #its = InfluencerTwitchStat.objects.filter(view_count__gte=obj.followers_count).distinct('influencer_user_profile')
        #for it in its:
        #    delta = 1
        #    delta_view_count = TwitchDeltaViewCount.objects.filter(
        #        influencer_user_profile=it.influencer_user_profile,
        #        campaign_type=obj.type).order_by(
        #        '-id').first()
        #    if delta_view_count:
        #        delta = delta_view_count.delta
        #    calculate_avg_estimated_payout(it.influencer_user_profile, obj, delta)
        return obj

    class Meta:
        model = Campaign
        # depth = 2
        fields = (
            'id', 'name', 'campaign_budget', 'campaign_logo', 'logo_position', 'campaign_script', 'type', 'expiry',
            'start_date',
            'end_date', 'remaining_budget', 'followers_count', 'description', 'created_on', 'updated_on',
            'status', 'status_last_updated', 'brand', 'campaign_brand_name', 'campaign_audio_text_file', 'campaign_underlay_url',
            'influencer_count', 'escrow_amount', 'paid_out_amount', 'num_of_days', 'influencer_status', 'submitted_media')

    def update(self, instance, validated_data):

        old_status = instance.status
        instance.status = validated_data.get('status', instance.status)
        if old_status != instance.status:
            instance.status_last_updated = datetime.datetime.today()

        instance.description = validated_data.get('description', instance.description)
        instance.save()
        print('after saving', instance.description, instance.status, old_status)

        return instance
