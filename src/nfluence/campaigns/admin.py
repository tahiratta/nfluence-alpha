from django.contrib import admin
from campaigns.models import Campaign


@admin.register(Campaign)
class CampaignAdmin(admin.ModelAdmin):

    def brand_name(self,obj):
        return obj.brand.name

    list_display = ('name', 'campaign_budget', 'type', 'followers_count', 'brand_name')
    # add filtering by campaign_budget
    list_filter = ('campaign_budget',)
    # add search fields
    search_fields = ['name', 'campaign_budget']
