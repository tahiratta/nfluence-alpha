from rest_framework import routers
from django.urls import path, include
from campaigns.views import (
    CampaignViewSet,
    CampaignInfluencerView,
    CampaignsHistoryView
)


router = routers.DefaultRouter()
router.register(r'campaigns', CampaignViewSet, basename="Campaign")


urlpatterns = [
    path('', include(router.urls)),
    path('campaigns/<id>/influencers/', CampaignInfluencerView.as_view()),
    path('historical-campaigns-report/', CampaignsHistoryView.as_view()),
]
