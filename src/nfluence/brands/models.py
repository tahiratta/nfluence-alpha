from django.db import models
from django.contrib.postgres.fields import citext
from nfluence.models import NfluenceBaseModel
from django.conf import settings
from django.core.files.storage import FileSystemStorage

UPLOAD_ROOT = settings.BASE_DIR + "/media_1/web_media/"
upload_storage = FileSystemStorage(location=UPLOAD_ROOT, base_url='/web_media_1')

class Brand(NfluenceBaseModel):
    name = citext.CICharField(unique=True, max_length=255)
    brand_company_legal_name = citext.CICharField(max_length=255, blank=True, null=True)
    brand_logo = models.ImageField(upload_to='brands_logos/', storage=upload_storage, blank=True, null=True)
    #brand_logo = models.ImageField(upload_to='brands_logos/', blank=True, null=True)
    address_line_1 = citext.CICharField(max_length=255, blank=True, null=True)
    address_line_2 = citext.CICharField(max_length=255, blank=True, null=True)
    city = citext.CICharField(max_length=255, blank=True, null=True)
    state = citext.CICharField(max_length=255, blank=True, null=True)
    zip_code = citext.CICharField(max_length=255, blank=True, null=True)
    country = citext.CICharField(max_length=255, blank=True, null=True)
    phone = citext.CICharField(max_length=255, blank=True, null=True)
    paypal_account = citext.CICharField(max_length=255, blank=True, null=True, unique=True)

    def clean(self):
        self.name = self.name.capitalize()

    # def get_brand_remaining_budget(self):
    #     total_amount_transferred = 0
    #     total_allocated_campaign_budget = 0
    #
    #     for transfer in self.nfluenceaccount_set.all():
    #         total_amount_transferred += transfer.amount_transferred
    #
    #     for campaign in self.campaign_set.all():
    #         if campaign.campaign_budget is not None:
    #             total_allocated_campaign_budget += campaign.campaign_budget
    #     return total_amount_transferred - total_allocated_campaign_budget

    def get_active_brand_campaigns_remaining_budget(self):
        total_allocated_campaign_budget = 0
        total_campaign_escrow_amount = 0
        total_campaign_paid_amount = 0

        for campaign in self.campaign_set.filter(status="active"):
            #total budget calc
            # if campaign.status == "active":
            if campaign.campaign_budget is not None:
                total_allocated_campaign_budget += campaign.campaign_budget

            # escrow calc
            for esc in campaign.escrow_set.all():

                if esc.transaction_status == 'paid':
                    if esc.amount_actual:
                        total_campaign_paid_amount += esc.amount_actual
                    elif esc.amount_estimated:
                        total_campaign_paid_amount += esc.amount_estimated
                elif esc.transaction_status == 'pending_processing':
                    if esc.amount_actual:
                        total_campaign_escrow_amount += esc.amount_actual
                    elif esc.amount_estimated:
                        total_campaign_escrow_amount += esc.amount_estimated
            # total_campaign_escrow_amount = total_campaign_escrow_amount * 10000
            # total_campaign_paid_amount = total_campaign_paid_amount * 10000

        # return total_allocated_campaign_budget - (total_campaign_escrow_amount * 100) - (total_campaign_paid_amount * 100)
        return total_allocated_campaign_budget - total_campaign_escrow_amount - total_campaign_paid_amount

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name


class BrandContact(NfluenceBaseModel):
    PRIMARY_PHONE_CHOICES = (
        ('cell_phone', 'Cell Phone'),
        ('direct_phone', 'Direct Phone'),
        ('other_phone', 'Other Phone')
    )

    title = citext.CICharField(max_length=255, blank=True, null=True)
    name = citext.CICharField(max_length=255, blank=True, null=True)
    email = citext.CIEmailField(max_length=70, blank=True, null=True)

    cell_phone = citext.CICharField(max_length=255, blank=True, null=True)
    direct_phone = citext.CICharField(max_length=255, blank=True, null=True)
    other_phone = citext.CICharField(max_length=255, blank=True, null=True)
    primary_phone = citext.CICharField(max_length=255, blank=True, null=True, choices=PRIMARY_PHONE_CHOICES)

    city = citext.CICharField(max_length=255, blank=True, null=True)
    state = citext.CICharField(max_length=255, blank=True, null=True)
    zip_code = citext.CICharField(max_length=255, blank=True, null=True)
    country = citext.CICharField(max_length=255, blank=True, null=True)

    brand = models.ForeignKey(Brand, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name
