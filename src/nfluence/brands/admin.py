from django.contrib import admin
from brands.models import Brand, BrandContact

admin.site.register(BrandContact)
@admin.register(Brand)
class BrandAdmin(admin.ModelAdmin):
    list_display = ('name', 'brand_company_legal_name', 'paypal_account')
    # add search fields
    search_fields = ['name', 'brand_company_legal_name', 'paypal_account']
