from rest_framework import routers
from django.urls import path, include
from brands.views import (
    BrandViewSet, BrandContactViewSet, BrandListViewSet
)

router = routers.DefaultRouter()
router.register(r'brands', BrandViewSet)
router.register(r'brands_list', BrandListViewSet)
router.register(r'brand_contact', BrandContactViewSet)
router.register(r'brands/<brand_id>/campaigns', BrandViewSet)


urlpatterns = [
    path('', include(router.urls))
]
