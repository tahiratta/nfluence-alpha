from rest_framework import serializers
from brands.models import (
    Brand, BrandContact
)
from campaigns.serializers import (
    CampaignSerializer
)
from campaigns.models import Campaign
from django.db.models import Sum
from payments.models import Escrow

class BrandContactSerializer(serializers.ModelSerializer):
    brand_id = serializers.IntegerField(write_only=True, required=False)

    class Meta:
        model = BrandContact
        depth = 1

        fields = ('id', 'title', 'name', 'email', 'brand_id',
                  'cell_phone', 'direct_phone', 'other_phone', 'primary_phone',
                  'city', 'state', 'zip_code', 'country'
                  )

    def create(self, validated_data):
        brand_id = validated_data.pop('brand_id', None)
        if not brand_id:
            return self.error_messages
        bc = BrandContact.objects.create(**validated_data, brand_id=brand_id)
        return bc


class BrandListSerializer(serializers.ModelSerializer):

    class Meta:
        model = Brand
        depth = 1

        fields = ('id', 'name'
                  )


class BrandSerializer(serializers.ModelSerializer):
    total_active_brand_campaigns = serializers.SerializerMethodField()
    total_active_brand_campaigns_budget = serializers.SerializerMethodField()
    total_active_brand_campaigns_escrow = serializers.SerializerMethodField()
    total_active_brand_campaigns_paid = serializers.SerializerMethodField()
    brand_contacts = serializers.SerializerMethodField()
    brandcontact_set = BrandContactSerializer(write_only=True, many=True)
    campaign_set = CampaignSerializer(read_only=True, many=True)
    brand_logo = serializers.ImageField(required=True)

    @staticmethod
    def get_total_active_brand_campaigns(obj):
        return Campaign.objects.filter(brand_id=obj.id, status='active').count()

    @staticmethod
    def get_total_active_brand_campaigns_budget(obj):
        return Campaign.objects.filter(brand_id=obj.id, status='active').aggregate(Sum('campaign_budget'))[
            'campaign_budget__sum']

    @staticmethod
    def get_total_active_brand_campaigns_escrow(obj):
        sum_ = 0
        campaigns = Campaign.objects.values_list('id', flat=True).filter(brand_id=obj.id, status='active')
        escrow_campaigns = Escrow.objects.values('amount_actual', 'amount_estimated').filter(campaign_id__in=list(campaigns), transaction_status='pending_processing')
        for esc in escrow_campaigns:
            if esc['amount_actual']:
                sum_ += esc['amount_actual']
            elif esc['amount_estimated']:
                sum_ += esc['amount_estimated']
        #campaigns = Campaign.objects.filter(brand_id=obj.id, status='active')
        #for campaign in campaigns:
        #    escrow_campaigns = Escrow.objects.filter(campaign_id=campaign.id, transaction_status='pending_processing')
        #    for esc in escrow_campaigns:
        #        if esc.amount_actual:
        #            sum_ += esc.amount_actual
        #        elif esc.amount_estimated:
        #            sum_ += esc.amount_estimated
        return sum_

    @staticmethod
    def get_total_active_brand_campaigns_paid(obj):
        sum_ = 0
        campaigns = Campaign.objects.values_list('id', flat=True).filter(brand_id=obj.id, status='active')
        escrow_campaigns = Escrow.objects.values('amount_actual', 'amount_estimated').filter(campaign_id__in=list(campaigns), transaction_status='paid')
        for esc in escrow_campaigns:
            if esc['amount_actual']:
                sum_ += esc['amount_actual']
            elif esc['amount_estimated']:
                sum_ += esc['amount_estimated']
        #campaigns = Campaign.objects.values('id').filter(brand_id=obj.id, status='active')
        #for campaign in campaigns:
        #    escrow_campaigns = Escrow.objects.filter(campaign_id=campaign['id'], transaction_status='paid')
        #    for esc in escrow_campaigns:
        #        if esc.amount_actual:
        #            sum_ += esc.amount_actual
        #        elif esc.amount_estimated:
        #            sum_ += esc.amount_estimated 
        return sum_

    @staticmethod
    def get_brand_contacts(obj):
        return [BrandContactSerializer(i).data for i in obj.brandcontact_set.all()]

    def create(self, validated_data):
        brand_contacts = validated_data.pop('brandcontact_set', None)
        brand = Brand.objects.create(**validated_data)
        for bc in brand_contacts:
            bc['brand_id'] = brand.id
            BrandContact.objects.create(**bc)
        return brand

    class Meta:
        depth = 1
        model = Brand
        fields = ('id', 'name', 'brand_company_legal_name', 'brand_logo',
                  'address_line_1', 'address_line_2', 'city', 'state', 'zip_code', 'country',
                  'phone', 'paypal_account',
                  # 'contact_phone', 'email', 'contact_phone_2', 'email_2',
                  'total_active_brand_campaigns', 'total_active_brand_campaigns_budget', 'get_active_brand_campaigns_remaining_budget',
                  'total_active_brand_campaigns_escrow', 'total_active_brand_campaigns_paid',
                  'campaign_set', 'brand_contacts', 'brandcontact_set',
                  )
