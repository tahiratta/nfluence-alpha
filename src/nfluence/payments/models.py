from django.db import models
from django.contrib.postgres.fields import citext
from nfluence.models import NfluenceBaseModel
from brands.models import Brand
from accounts.models import (
    InfluencerUserProfile,
    UserProfile
)
from campaigns.models import Campaign


class NfluenceAccount(NfluenceBaseModel):

    amount_transferred = models.IntegerField(blank=True, null=True)
    brand = models.ForeignKey(Brand, on_delete=models.CASCADE)


class Escrow(NfluenceBaseModel):

    amount_estimated = models.DecimalField(blank=True, null=True, max_digits=10, decimal_places=2)
    amount_actual = models.DecimalField(blank=True, null=True, max_digits=10, decimal_places=2)
    transaction_status = citext.CICharField(max_length=255, blank=True, null=True)
    payment_mature_status = citext.CICharField(max_length=255, default="immatured")
    influencer_user_profile = models.ForeignKey(InfluencerUserProfile, on_delete=models.CASCADE)
    campaign = models.ForeignKey(Campaign, on_delete=models.CASCADE)

class WithdrawAmount(NfluenceBaseModel):
    withdraw_amount = models.DecimalField(default=0.00, max_digits=10, decimal_places=2)
    available_balance = models.DecimalField(default=0.00, max_digits=10, decimal_places=2)
    user_profile = models.ForeignKey(UserProfile, on_delete=models.CASCADE)
    campaign = models.ForeignKey(Campaign, on_delete=models.CASCADE, null=True, blank=True)
    matured_amount = models.DecimalField(blank=True, null=True, max_digits=10, decimal_places=2)

# class AvailableBalance(NfluenceBaseModel):
#     available_balance = models.IntegerField(default=0)
#     influencer_user_profile = models.ForeignKey(InfluencerUserProfile, on_delete=models.CASCADE)

class PayoutItems(NfluenceBaseModel):
    payout_batch_id = models.CharField(max_length=255, blank=True, null=True)
    payout_item_id = models.CharField(max_length=255, blank=True, null=True)
    transaction_status = models.CharField(max_length=255, blank=True, null=True)
    withdraw_amount = models.CharField(max_length=255, blank=True, null=True)
    email = models.CharField(max_length=255, blank=True, null=True)
