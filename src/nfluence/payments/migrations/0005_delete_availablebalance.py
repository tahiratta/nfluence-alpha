# Generated by Django 2.2.5 on 2020-03-06 10:03

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('payments', '0004_auto_20200306_1303'),
    ]

    operations = [
        migrations.DeleteModel(
            name='AvailableBalance',
        ),
    ]
