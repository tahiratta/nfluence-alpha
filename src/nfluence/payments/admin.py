from django.contrib import admin
from payments.models import (
    NfluenceAccount,
    Escrow,
    WithdrawAmount,
    # AvailableBalance,
    PayoutItems
)

admin.site.register(WithdrawAmount)
admin.site.register(PayoutItems)
# admin.site.register(AvailableBalance)
@admin.register(NfluenceAccount)
class NfluenceAccountAdmin(admin.ModelAdmin):

    def brand_name(self, obj):
        return obj.brand.name

    list_display = ('amount_transferred',  'brand_name')
    # add filtering by yearly_budget
    # list_filter = ('amount_transferred', 'brand_name')
    # add search fields
    search_fields = ['brand_name']


@admin.register(Escrow)
class EscrowAdmin(admin.ModelAdmin):

    def campaign_name(self, obj):
        return obj.campaign.name

    list_display = (
    'amount_estimated', 'amount_actual', 'transaction_status', 'influencer_user_profile', 'campaign_name')
    # add filtering by yearly_budget
    list_filter = ('amount_estimated', 'amount_actual',)
    # add search fields
    search_fields = ['amount_estimated', 'amount_actual', 'transaction_status', 'campaign_name']
